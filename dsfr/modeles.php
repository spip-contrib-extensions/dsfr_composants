<?php
/**
 * Fonctions du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Charge la configuration/description d'un modèle disponible pour le DSFR
 * 
 * @param string $nom_du_modele
 * 
 * @return array|bool
 *     - array : configuration/description du modèle
 *     - false : modèle invalide/introuvable
 **/
function dsfr_modeles_modele_dist($nom_du_modele) {
	static $modeles = [];

	if ( !is_string($nom_du_modele) ) {
		return false;
	}

	$nom = strtolower(trim($nom_du_modele));

	if ( empty($nom) ) {
		return false;
	}

	// déjà en chargé ?
	if ( isset($modeles[$nom]) ) {
		return $modeles[$nom];
	}

	$modele = false;

	$prefix = 'dsfr_modele_'.$nom;

	// il y a un fichier php associé au raccourci ? 
	if ( find_in_path('dsfr_modeles/'.$nom.'.php') ) {
		include_spip('dsfr_modeles/'.$nom);
	}

	// est-ce que le modèle est valide ?
	if ( function_exists($prefix.'_traiter') || trouver_fond($nom, 'dsfr_modeles/') ) {
		$modele = [
			'id'			=> $nom,
			'prefix'		=> $prefix,
		];

		// informations et configurations du modèle
		if ( function_exists($prefix) ) {
			$modele = array_merge($modele, $prefix());
		}
	}

	// stockage `static` pour éviter de re-charger à chaque fois si il y a plusieurs appels à cette fonction
	$modeles[$nom] = $modele;

	return $modele;
}

/**
 * Liste de tous les modèles disponibles pour le DSFR
 *  
 * @return array $modeles
 **/
function dsfr_modeles_liste_dist() {
	static $modeles;

	// déjà chargé ?
	if ( is_array($modeles) ) {
		return $modeles;
	}

	$fonction_dsfr_modeles_modele = charger_fonction('modele', 'dsfr/modeles');

	$modeles = [];
	foreach ( find_all_in_path('dsfr_modeles/', '[.](php|html)$') as $chemin ) {
		$pathinfo = pathinfo($chemin);
		$nom = basename($chemin, '.'.$pathinfo['extension']);

		// déjà chargé ?
		if ( array_key_exists($nom,$modeles) ) {
			continue;
		}

		if ( $modele = $fonction_dsfr_modeles_modele($nom) ) {
			$modeles[$nom] = $modele;
		}
	}

	ksort($modeles);

	return $modeles;
}

/**
 * Traitement spécifique d'un modèle DSFR
 *
 * @param string $nom_du_modele
 * 
 * @param array $contexte
 * @param bool $raccourci
 * 
 * @return string
 *     Contenu du traitement du modèle (ou pas ^^)
 **/
function dsfr_modeles_traiter_modele_dist($nom_du_modele, $contexte = [], $raccourci = false) {
	// retour de la fonction
	$retour = '';

	// nom du modèle toujours en minuscule
	$nom = strtolower(trim($nom_du_modele));

	// message d'erreur ?
	$erreur = false;

	// message attention ?
	$attention = false;

	// message d'information ?
	$message = false;

	// debug ?
	$debug = false;

	// retour du traitement d'un modèle
	$data = null;

	// charge le modèle et vérifie sa validité
	$fonction_dsfr_modeles_modele = charger_fonction('modele', 'dsfr/modeles');
	if ( $modele = $fonction_dsfr_modeles_modele($nom) ) {

		// ajoute le nom du modèle au contexte
		$contexte['dsfr_modele'] = $nom;

		// fonction de traitement (normalement déjà préchargée)
		$fonction_traiter_modele = $modele['prefix'].'_traiter';
		if ( function_exists($fonction_traiter_modele) ) {
			$traitement_fonction = $fonction_traiter_modele($contexte);

			// un string, pas de gestion d'erreur/avertissement possible, on considère le traitement ok
			if ( is_string($traitement_fonction) ) {
				$data = $traitement_fonction;
			}
			else if ( is_array($traitement_fonction) ) {
				// il y a une erreur
				if ( array_key_exists('erreur', $traitement_fonction) ) {
					$erreur = $traitement_fonction['erreur'];
				}
				// il y a un avertissement
				if ( array_key_exists('attention', $traitement_fonction) ) {
					$attention = $traitement_fonction['attention'];
				}
				// il y a un message
				if ( array_key_exists('message', $traitement_fonction) ) {
					$message = $traitement_fonction['message'];
				}
				// il y a un debug
				if ( array_key_exists('debug', $traitement_fonction) ) {
					$debug = $traitement_fonction['debug'];
				}
				// retour du traitement uniquement si aucune erreur
				if ( !$erreur && array_key_exists('data', $traitement_fonction) ) {
					$data = $traitement_fonction['data'];
				}
			}
		}
		// traitement d'un squelette simple
		else if ( trouver_fond($nom, 'dsfr_modeles/') ) {
			$data = trim(recuperer_fond('dsfr_modeles/'.$nom, $contexte));
		}
	}
	else {
		$erreur = _T('dsfr_composants:modele_dsfr_invalide');
	}

	if ( !$erreur && is_null($data) ) {
		$erreur = _T('dsfr_composants:erreur_de_traitement_du_modele_dsfr');
	}

	if ( !$erreur && !$attention && empty($data) ) {
		$attention = _T('dsfr_composants:le_modele_dsfr_n_a_produit_aucun_contenu');
	}

	// le DSFR n'est pas disponible sur l'espace privé
	// remplace le modèle par un message
	if ( test_espace_prive() ) {
		$retour = '<div class="dsfr_modele" data-nom="'.attribut_html($nom).'">';
		// il y a une erreur
		if ( $erreur ) {
			$retour .= '<div class="dsfr_modele_erreur">';
			$retour .= '<p>'.(is_array($erreur) ? implode('</p><p>',$erreur) : $erreur).'</p>';
			$retour .= '</div>';
		}
		// il y a un message d'avertissement
		if ( $attention ) {
			$retour .= '<div class="dsfr_modele_attention">';
			$retour .= '<p>'.(is_array($attention) ? implode('</p><p>',$attention) : $attention).'</p>';
			$retour .= '</div>';
		}

		// affiche le code de raccourci du modèle
		if ( $raccourci ) {
			$parametres_raccourci_format_d_origine = [];
			foreach ( $contexte['parametres_raccourci'] AS $cle => $valeur ) {
				if ( $cle != $valeur ) {
					$parametres_raccourci_format_d_origine[] = $cle.'='.$valeur;
				}
				else {
					$parametres_raccourci_format_d_origine[] = $cle;
				}
			}

			// ajoute le nom si il n'est pas déjà dans les paramètres (ex. `<dsfr|composant=alerte|...>`)
			if ( !empty($nom) && $nom != array_key_first($contexte['parametres_raccourci']) ) {
				array_unshift($parametres_raccourci_format_d_origine, $nom);
			}

			$separation = count($parametres_raccourci_format_d_origine) > 3 ? "\n" : '';

			$retour .= '<div class="dsfr_modele_parametres_raccourci">';
			$retour .= spip_balisage_code('<dsfr'.(!empty($parametres_raccourci_format_d_origine) ? $separation.'|'.implode($separation.'|',$parametres_raccourci_format_d_origine).$separation : '').'>', true, '', 'spip');
			$retour .= '</div>';
		}

		// il y a un message
		if ( $message ) {
			$retour .= '<div class="dsfr_modele_message">';
			$retour .= '<p>'.(is_array($message) ? implode('</p><p>',$message) : $message).'</p>';
			$retour .= '</div>';
		}

		// ajoute le debug
		if ( $debug ) {
			$retour .= '<div class="dsfr_modele_debug">';

			// debug autre que booléen
			if ( is_string($debug) ) {
				$retour .= '<p>'.$debug.'</p>';
			}
			else if ( is_array($debug) ) {
				$retour .= spip_balisage_code(var_export($debug,1), true, '', 'php');
			}

			// debug du contexte
			$retour .= spip_balisage_code(var_export($contexte,1),1,'','php');

			// debug html
			if ( !empty($data) ) {
				$retour .= spip_balisage_code($data,1,'','html');
			}

			$retour .= '</div>';
		}

		$retour .= '</div>';
	}
	// espace public sans erreur
	else if ( !$erreur ) {
		$retour = $data;
	}

	return $retour;
}

/**
 * Retrouve l'`id` d'un objet à partir des paramètres d'un modèle
 * 
 * @param array $parametres
 * @param string $type_objet
 * @param bool $liste
 *  
 * @return mixed int|array|false
 **/
function dsfr_modeles_parametres_id_objet($parametres, $type_objet, $liste = false) {

	$id_trouves = [];

	$type_objet = strtolower($type_objet);
	$type_objet_raccourci = substr($type_objet, 0, 3); // `article` => `art`

	foreach ( $parametres AS $parametre => $valeur ) {
		if ( is_string($valeur) ) {
			$valeur = strtolower($valeur);
			if (
				str_starts_with($valeur, $type_objet)
				AND $id = substr($valeur, strlen($type_objet))
				AND intval($id) > 0
			) {
				$id_trouves[] = intval($id);
			}
			else if (
				str_starts_with($valeur, $type_objet_raccourci)
				AND $id = substr($valeur, strlen($type_objet_raccourci))
				AND intval($id) > 0
			) {
				$id_trouves[] = intval($id);
			}
		}
	}

	return $liste ? array_unique($id_trouves) : end($id_trouves);
}