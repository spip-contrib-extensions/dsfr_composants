<?php
/**
 * Options du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/******************************************************************************************
 * CONFIGURATIONS DU PLUGIN
 * 
 * Variables et constantes de personnalisation dédiées au PLUGIN
 * Voir : https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/developpeurs/prise-en-main
 **/

/*
	Les classes utilitaires, notamment les icônes, sont disponibles dans un fichier à part nommé `utility.css`
*/
defined('_DSFR_CHARGER_CSS_UTILITY') || define('_DSFR_CHARGER_CSS_UTILITY', true);

/*
	Le mode sélectionné permet de déterminer le contexte dans lequel doit fonctionner le DSFR
	et de modifier son comportement.

	Voir : https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/developpeurs/api-javascript/

	Valeurs possibles :
	 - `auto` (par défaut) choisi automatiquement le mode le plus approprié
	 - `manual` le lancement du moteur est laissé à l'utilisateur via la commande : `dsfr.start();`
	 - `runtime` le lancement se fait immédiatement dès l'initialisation du DSFR
	 - `loaded` le lancement se fait une fois la page totalement chargée
*/
defined('_DSFR_API_JS_MODE') || define('_DSFR_API_JS_MODE', 'auto');

/*
	Le mode `verbose` est une option disponible dans le DSFR qui fournit des détails supplémentaires 
	sur ce que fait le framework JS au démarrage ou lors de la navigation, il produit une sortie détaillée à des fins de diagnostic.
*/
defined('_DSFR_API_JS_VERBOSE') || define('_DSFR_API_JS_VERBOSE', false);

/*
	La propriété `level` dans la configuration ou en paramètre de l'URL permet de mettre un niveau de log particulier.

	Différents niveaux sont disponibles permettant une granularité plus fine des messages dans la console :
	- `log` toutes les actions opérées en js sont décrites
	- `debug` les actions principales sont décrites (création et destruction d'instance, démarrage et arrêt de l'API)
	- `info` niveau par défaut
	- `warn` affiche les avertissements
	- `error` affiche uniquement les erreurs
*/
defined('_DSFR_API_JS_LEVEL') || define('_DSFR_API_JS_LEVEL', 'info');

/*
	Le mode `production` lorsqu'il est activé, permet de retirer tous les logs de la console. 
	Par défaut, même en mode `verbose:false`, les logs d'initialisation du DSFR et des analytics sont présent dans la console.
	Ce mode est à mettre à `true` sur la version du site en production (disponible au public).
*/
defined('_DSFR_API_JS_PRODUCTION') || define('_DSFR_API_JS_PRODUCTION', false);

/*
	Le marquage du Système de Design de l'État s'inscrit dans le cadre de la gestion du marché de mesure 
	d'audience au niveau interministériel pour les sites et applications de l'État.

	Le système de design apporte avec lui un outil de collecte de données analytics basé sur la solution Eulérian.

	Voir : https://www.systeme-de-design.gouv.fr/utilisation-et-organisation/developpeurs/api-analytics/

	Pour activer le fonctionnement du package Analytics, une configuration particulière du DSFR est nécessaire.
	Exemple de configuration : https://github.com/GouvernementFR/dsfr/blob/main/src/analytics/doc/analytics/installation/configuration.md

	Pour le plugin, il faut définir la configuration sous forme de tableau PHP :

	```
	define('_DSFR_API_ANALYTICS', [
		'domain'	=> 'mon.domainedetracking.com',
		'page'		=> [
			'template'		=> 'article',
		],
		'site'		=> [
			'entity'		=> 'Ministère de l\'éducation nationale',
		],
	]);
	```
*/
defined('_DSFR_API_ANALYTICS') || define('_DSFR_API_ANALYTICS', false);


/******************************************************************************************
 * FONCTIONS DU PLUGIN
 * 
 * Fonctions du plugin chargées systématiquement
 **/
include_spip('inc/dsfr');
include_spip('inc/dsfr_composants');


/******************************************************************************************
 * CONFIGURATIONS SPIP
 * 
 * Variables et constantes de personnalisation dédiées à SPIP
 * Voir : https://www.spip.net/fr_rubrique643.html
 **/

// ajoute le marqueur DSFR
$GLOBALS['marqueur_skel'] = ($GLOBALS['marqueur_skel'] ?? '').dsfr_marqueur();

// des puces DSFR sur l'espace public uniquement
$GLOBALS['puce'] = '<span class="fr-icon-arrow-right-s-line" aria-hidden="true"></span>';

// concerne uniquement l'espace public
if ( !test_espace_prive() ) {
	// change le format des notes pour la mise en conformité DSFR
	defined('_NOTES_OUVRE_REF') || define('_NOTES_OUVRE_REF', '<sup class="dsfr_reference_note">');
	defined('_NOTES_FERME_REF') || define('_NOTES_FERME_REF', '</sup>');
	defined('_NOTES_OUVRE_NOTE') || define('_NOTES_OUVRE_NOTE', '<span class="dsfr_reference_note">');
	defined('_NOTES_FERME_NOTE') || define('_NOTES_FERME_NOTE', '&nbsp;</span>');

	// nombre de liens de pagination maximum par défaut
	defined('_PAGINATION_NOMBRE_LIENS_MAX') || define('_PAGINATION_NOMBRE_LIENS_MAX', 6);
}