<?php
/**
 * Fonctions du plugin chargées au calcul des squelettes
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

// chargement de tous les filtres disponibles pour le DSFR
dsfr_charger_filtres();

if ( _request('exec') == 'documentation_dsfr_composants' ) {
	include_spip('inc/dsfr_composants_documentation');
}