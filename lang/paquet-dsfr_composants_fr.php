<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		MIT - https://github.com/GouvernementFR/dsfr/blob/main/LICENSE.md
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// D
	'dsfr_composants_description' => '
Le plugin {{DSFR Composants}} charge automatiquement sur l\'espace public les scripts CSS et Javascript permettant d\'utiliser l\'ensemble des composants DSFR. Vous devrez ensuite adapter le contenu de vos squelettes pour pouvoir utiliser ces différents composants.

Sur l\'espace public, le plugin effectue les modifications suivantes :

{{Ajoute à la balise `<html>`}}
-*  data-fr-scheme="system"

{{Ajoute dans la balise `<head>`}}
-*  meta: charset,viewport et theme-color
-* favicon + manifest.webmanifest
-* /dsfr/dsfr.min.css
-* /dsfr/utility/utility.min.css (optionnel)
-* /css/dsfr_composants.css (styles CSS spécifiques au plugin)
-* /css/dsfr_icones.css (optionnel, seulement si vous avez ajouté des icônes dans le dossier `dsfr_icones/`)

{{Ajoute juste avant la fermeture de la balise `</body>`}}
-* /dsfr/dsfr.module.min.js
-* /dsfr/dsfr.nomodule.min.js
-* /dsfr/analytics/analytics.module.min.js (optionnel)
-* /dsfr/analytics/analytics.nomodule.min.js (optionnel)
-* /js/dsfr_composants.js (fonctions Javascript spécifiques au plugin)
	',
	'dsfr_composants_nom' => 'DSFR Composants',
	'dsfr_composants_slogan' => 'Composants du Système de Design de l\'État',
);