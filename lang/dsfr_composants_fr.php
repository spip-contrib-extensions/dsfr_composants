<?php
/**
 * Fichier langue de SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		MIT - https://github.com/GouvernementFR/dsfr/blob/main/LICENSE.md
 * 
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'a_propos_des_cookies' => 'À propos des cookies',
	'a_propos_des_cookies_texte_bandeau_de_consentement' => 'Bienvenue ! Nous utilisons des cookies pour améliorer votre expérience et les services disponibles sur ce site. Vous pouvez, à tout moment, avoir le contrôle sur les cookies que vous souhaitez activer.',
	'acces_rapide' => 'Accès rapide',
	'accueil' => 'Accueil',
	'accepter' => 'Accepter',
	'autoriser_tous_les_cookies' => 'Autoriser tous les cookies',

	// C
	'choisissez_un_theme_pour_personnaliser_l_apparence_du_site' => 'Choisissez un thème pour personnaliser l\'apparence du site.',
	'confirmer_mes_choix' => 'Confirmer mes choix',
	'cookies_obligatoires' => 'Cookies obligatoires',
	'cookies_obligatoires_descriptif' => 'Ce site utilise des cookies nécessaires à son bon fonctionnement qui ne peuvent pas être désactivés.',

	// D
	'dsfr' => 'DSFR',
	'dsfr_composants' => 'DSFR Composants',

	// E
	'erreur_de_traitement_du_modele_dsfr' => 'Erreur de traitement du modèle DSFR !',
	'extrait_de_code' => 'Extrait de code',

	// F
	'fermer' => 'Fermer',

	// L
	'le_parametre_est_obligatoire' => 'Le paramètre @nom_du_parametre@ est obligatoire.',
	'le_parametre_est_vide' => 'Le paramètre @nom_du_parametre@ est vide.',
	'le_parametre_est_invalide' => 'Le paramètre @nom_du_parametre@ est invalide.',
	'le_parametre_est_manquant' => 'Le paramètre @nom_du_parametre@ est manquant.',
	'le_modele_dsfr_n_a_produit_aucun_contenu' => 'Le modèle DSFR n\'a produit aucun contenu !',

	// M
	'masquer_le_message' => 'Masquer le message',
	'menu_principal' => 'Menu principal',
	'merci_de_reessayer_plus_tard' => 'Merci de réessayer plus tard.',
	'modele_dsfr' => 'Modèle DSFR',
	'modele_dsfr_invalide' => 'Modèle DSFR invalide !',

	// O
	'ouvre_une_nouvelle_fenetre' => 'ouvre une nouvelle fenêtre',

	// P
	'pagination' => 'Pagination',
	'pagination_derniere_page' => 'Dernière page',
	'pagination_page_nb' => 'Page @nb@',
	'pagination_page_precedente' => 'Page précédente',
	'pagination_page_suivante' => 'Page suivante',
	'pagination_premiere_page' => 'Première page',
	'panneau_de_gestion_des_cookies' => 'Panneau de gestion des cookies',
	'parametres_d_affichage' => 'Paramètres d\'affichage',
	'personnaliser' => 'Personnaliser',
	'personnaliser_les_cookies' => 'Personnaliser les cookies',
	'preferences_pour_tous_les_services' => 'Préférences pour tous les services.',

	// R
	'recherche' => 'Recherche',
	'rechercher' => 'Rechercher',
	'refuser' => 'Refuser',
	'refuser_tous_les_cookies' => 'Refuser tous les cookies',
	'retirer_filtre' => 'Retirer @filtre@',
	'revenir_plus_haut' => 'Revenir plus haut',

	// S
	'service_indisponible' => 'Service indisponible',
	'service_indisponible_message' => 'Le service "@nom_du_service@" rencontre un problème, nous travaillons pour le résoudre le plus rapidement possible.',
	'sommaire' => 'Sommaire',
	'systeme' => 'Système',

	// T
	'theme_clair' => 'Thème clair',
	'theme_sombre' => 'Thème sombre',
	'tout_accepter' => 'Tout accepter',
	'tout_refuser' => 'Tout refuser',
	'trier_par_tri' => 'Trier par : @tri@',

	// U
	'utilise_les_parametres_systeme' => 'Utilise les paramètres système',

	// V
	'voir_le_fil_d_ariane' => 'Voir le fil d\'Ariane',
	'voir_toute_la_rubrique' => 'Voir toute la rubrique',
	'vous_etes_ici' => 'vous êtes ici :',
);