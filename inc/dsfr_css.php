<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Retourne un tableau de la liste des feuilles de styles CSS à inclure pour le DSFR.
 * 
 * @return array
 **/
function inc_dsfr_css_dist() {
	// nécessaire pour `timestamp()` et `produire_fond_statique()`
	include_spip('inc/filtres');

	// tableau des feuilles de styles CSS
	// clé = uid du fichier, valeur = balise `<link rel="stylesheet">`
	$css = [];

	// librairie DSFR
	$css['lib'] = '<link rel="stylesheet" href="'.timestamp(dsfr('dsfr.min.css')).'">';
	// utilitaires
	if ( _DSFR_CHARGER_CSS_UTILITY ) {
		$css['lib/utility'] = '<link rel="stylesheet" href="'.timestamp(dsfr('utility/utility.min.css')).'">';

		// si il y a des icônes DSFR perso
		if ( dsfr_liste_icones('type=perso') ) {
			$css['css/dsfr_icones'] = '<link rel="stylesheet" href="'.produire_fond_statique('css/dsfr_icones.css').'">';
		}
	}
	// dsfr
	$css['css/dsfr_composants'] = '<link rel="stylesheet" href="'.produire_fond_statique('css/dsfr_composants.css').'">';

	// pipeline permettant d'inclure/modifier les feuilles de styles
	$css = pipeline('dsfr_css', $css);

	return $css;
}