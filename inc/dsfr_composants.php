<?php
/**
 * Fonctions du plugin chargées par SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Recherche un fichier dans les chemins de SPIP de la lib DSFR
 * 
 * @param string $fichier
 *     Fichier recherché
 * 
 * @return mixed
 *     - string : chemin du fichier trouvé
 *     - false : fichier introuvable
 * 
 * @see documentation/balises/dsfr
 **/
function dsfr($fichier) {
	if ( !$fichier and !strlen($fichier) ) {
		return false;
	}

	return find_in_path('lib/dsfr/'.$fichier);
}

/**
 * Retourne la version du DSFR
 * 
 * @return string
 * 
 * @see documentation/balises/dsfr_version
 **/
function dsfr_version() {

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois
	if ( !isset($GLOBALS['dsfr_version']) ) {
		include_spip('inc/filtres');
		$info_plugin = charger_filtre('info_plugin');

		$GLOBALS['dsfr_version'] = $info_plugin('dsfr', 'version');
	}

	return $GLOBALS['dsfr_version'];
}

/**
 * Retourne un identifiant unique créé aléatoirement avec une longueur minimum de 4 caractères
 * 
 * ATTENTION : cette fonction ne garantit pas l'unicité de la valeur retournée.
 * Avec des identifiants de petite longueur. Il y a de forte chance qu'il ne soit pas unique.
 * 
 * Pour un identifiant vraiment unique, utilisez la fonction SPIP `creer_uniqid()` (par contre l'identifiant sera très long)
 *
 * @param int $longueur
 *     Longueur de l'identifiant créé
 *     Si le paramètre est égal à `-1`, retourne le dernier identifiant créé
 * 
 * @return string Identifiant
 */
function dsfr_uniqid($longueur = 13) {

	// renvoyer le dernier identifiant créé
	if ( $longueur == -1 ) {
		return isset($GLOBALS['dsfr_uniqid']) ? $GLOBALS['dsfr_uniqid'] : false;
	}

	// longueur
	$longueur = empty($longueur) ? 13 : intval($longueur);
	if ( $longueur < 4 ) {
		$longueur = 4;
	}

	if ( function_exists('random_bytes') ) {
		$aleatoire = bin2hex(random_bytes(ceil($longueur/2)));
	}
	else if ( function_exists('openssl_random_pseudo_bytes') ) {
		$aleatoire = bin2hex(openssl_random_pseudo_bytes(ceil($longueur/2)));
	}
	// au cas ou... utilise la fonction de SPIP
	else {
		if ( !function_exists('creer_uniqid') ) {
			include_spip('inc/acces');
		}
		$aleatoire = creer_uniqid();
	}

	// coupe à la bonne longueur
	$aleatoire = substr($aleatoire, 0, $longueur);

	// stockage en $GLOBALS pour permettre de retrouver le dernier identifiant créé
	$GLOBALS['dsfr_uniqid'] = $aleatoire;

	return $aleatoire;
}

/**
 * Retourne le marqueur DSFR pour la gestion du cache en fonction
 * du plugin et des cookies DSFR
 * 
 * @return string
 **/
function dsfr_marqueur() {
	$marqueur = ':dsfr-'.dsfr_version();

	// marqueur des cookies uniquement sur l'espace public
	if ( !test_espace_prive() ) {
		$marqueur_des_cookies = '';
		foreach ( $_COOKIE as $nom => $valeur ) {
			if ( strpos($nom, 'dsfr_') === 0 ) {
				$marqueur_des_cookies .= $nom.'='.$valeur;
			}
		}
	
		if ( !empty($marqueur_des_cookies) ) {
			$marqueur .= '='.md5($marqueur_des_cookies);
		}
	}

	return $marqueur;
}

/**
 * Recherche un placeholder dans le dossier `dsfr_placeholders/`.
 * 
 * Si aucune extension n'est précisée, la recherche s'effectuera dans l'ordre suivant :
 * - `dsfr_placeholders/$reference.svg`
 * - `dsfr_placeholders/$reference.webp`
 * - `dsfr_placeholders/$reference.png`
 * - `dsfr_placeholders/$reference.gif`
 * - `dsfr_placeholders/$reference.jpg`
 * 
 * @param string $reference
 *     Référence unique du placeholder recherché
 * 
 * @example
 *     dsfr_placeholder('1x1')
 *     dsfr_placeholder('1x1.png')
 *     dsfr_placeholder('nom_de_votre_placeholder') placé dans votre dossier `dsfr_placeholders/nom_de_votre_placeholder.svg`
 * 
 * @return mixed
 *     - string : chemin du placeholder trouvé
 *     - false : placeholder introuvable
 * 
 * @see documentation/balises/dsfr_placeholder
 **/
function dsfr_placeholder($reference) {
	if ( !$reference and !strlen($reference) ) {
		return false;
	}

	$extensions = ['svg','webp','png','gif','jpg'];

	$extension = pathinfo($reference, PATHINFO_EXTENSION);
	if ( !empty($extension) && in_array($extension,$extensions) ) {
		return find_in_path('dsfr_placeholders/'.$reference);
	}

	$chemin = false;

	foreach( $extensions as $extension ) {
		if ( $chemin = find_in_path('dsfr_placeholders/'.$reference.'.'.$extension) ) {
			break;
		}
	}

	return $chemin;
}

/**
 * Recherche un pictogramme DSFR dans le dossier `dsfr_pictogrammes/`.
 * Si aucun pictogramme n'est trouvé alors une recherche est effectuée dans le dossier `artwork/pictograms/` de la lib DSFR.
 * Permet ainsi de facilement surcharger ou ajouter de nouveaux pictogrammes en les plaçant simplement dans le dossier `dsfr_pictogrammes/`.
 * 
 * @param string $reference
 *     Référence unique du pictogramme recherché
 * 
 * @example
 *     dsfr_pictogramme('buildings/city-hall')
 *     dsfr_pictogramme('institutions/firefighter')
 *     dsfr_pictogramme('nom_de_votre_pictogramme') placé dans votre dossier `dsfr_pictogrammes/nom_de_votre_pictogramme.svg`
 * 
 * @return mixed
 *     - string : chemin du pictogramme trouvé
 *     - false : pictogramme introuvable
 * 
 * @see documentation/balises/dsfr_pictogramme
 **/
function dsfr_pictogramme($reference) {
	if ( !$reference and !strlen($reference) ) {
		return false;
	}

	$pictogrammes = dsfr_liste_pictogrammes();
	if ( array_key_exists($reference,$pictogrammes) && array_key_exists('chemin',$pictogrammes[$reference]) ) {
		return $pictogrammes[$reference]['chemin'];
	}

	return false;
}

/**
 * Liste des pictogrammes disponibles pour le DSFR
 * 
 * @param string $lister
 *     Voir la fonction `dsfr_composants_lister_tableau()` pour utiliser ce paramètre
 *
 * @example
 *     `dsfr_liste_pictogrammes('valeur_de_la_categorie')`
 *     `dsfr_liste_pictogrammes('categorie=valeur_de_la_categorie')`
 *     `dsfr_liste_pictogrammes('type=valeur_du_type')`
 *     
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `dsfr_liste_pictogrammes('liste=categorie')`
 *     `dsfr_liste_pictogrammes('liste=type')`
 *  
 * @return array $pictogrammes
 * 
 * @see documentation/balises/dsfr_pictogrammes
 **/
function dsfr_liste_pictogrammes($lister = null) {

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois
	if ( !isset($GLOBALS['dsfr_liste_pictogrammes']) ) {
		$pictogrammes = [];

		foreach ( find_all_in_path('lib/dsfr/artwork/pictograms/', '[.]svg$', true) as $chemin ) {
			list($avant, $apres) = explode('lib/dsfr/artwork/pictograms/', $chemin, 2);
			$cle = substr($apres, 0, -4); // enlève `.svg`

			$pictogrammes[$cle] = [
				'type'			=> 'dsfr',
				'categorie'		=> strpos($cle, '/') !== false ? current(explode('/', $cle, 2)) : '',
				'chemin'		=> $chemin,
			];
		}

		foreach ( find_all_in_path('dsfr_pictogrammes/', '[.]svg$', true) as $chemin ) {
			list($avant, $apres) = explode('dsfr_pictogrammes/', $chemin, 2);
			$cle = substr($apres, 0, -4); // enlève `.svg`

			$pictogrammes[$cle] = [
				'type'			=> 'perso',
				'categorie'		=> strpos($cle, '/') !== false ? current(explode('/', $cle, 2)) : '',
				'chemin'		=> $chemin,
			];
		}

		$pictogrammes = pipeline('configurer_dsfr_liste_pictogrammes', $pictogrammes);

		$GLOBALS['dsfr_liste_pictogrammes'] = $pictogrammes;
	}
	else {
		$pictogrammes = $GLOBALS['dsfr_liste_pictogrammes'];
	}

	return dsfr_composants_lister_tableau($pictogrammes, $lister);
}

/**
 * Liste des composants disponibles pour le DSFR
 *  
 * @return array $composants
 **/
function dsfr_liste_composants() {
	// stockage en $GLOBALS pour éviter de re-charger à chaque fois si il y a plusieurs appels à cette fonction lors de la requête
	if ( !isset($GLOBALS['dsfr_liste_composants']) ) {
		$composants = [];
		foreach ( find_all_in_path('dsfr_composants/', '[.]html$') as $chemin ) {
			$cle = basename($chemin, '.html');
			if ( strpos($cle, '.') === false ) { // ne pas lister les fichiers .demo / .js
				$composants[$cle] = $chemin;
			}
		}

		ksort($composants);

		$GLOBALS['dsfr_liste_composants'] = $composants;
	}
	else {
		$composants = $GLOBALS['dsfr_liste_composants'];
	}

	return $composants;
}

/**
 * Recherche une icône DSFR dans le dossier `dsfr_icones/`.
 * Si aucune icône n'est trouvée alors une recherche est effectuée dans le dossier `icons/` de la lib DSFR.
 * Permet ainsi de facilement surcharger ou ajouter de nouvelles icônes en les plaçant simplement dans le dossier `dsfr_icones/`.
 * 
 * @param string $reference
 *     Référence unique de l'icône recherchée
 * 
 * @example
 *     dsfr_icone('warning-fill')
 *     dsfr_icone('error-fill')
 *     dsfr_icone('account-fill')
 *     dsfr_icone('nom_de_votre_icone') placée dans votre dossier `dsfr_icones/nom_de_votre_icone.svg`
 * 
 * @return mixed
 *     - string : chemin de l'icône trouvée
 *     - false : icône introuvable
 * 
 * @see documentation/balises/dsfr_icone
 **/
function dsfr_icone($reference) {
	if ( !$reference and !strlen($reference) ) {
		return false;
	}

	$icones = dsfr_liste_icones();
	if ( array_key_exists($reference,$icones) && array_key_exists('chemin',$icones[$reference]) ) {
		return $icones[$reference]['chemin'];
	}

	return false;
}

/**
 * Liste des icônes disponibles pour le DSFR
 * 
 * @param string $lister
 *     Voir la fonction `dsfr_composants_lister_tableau()` pour utiliser ce paramètre
 *
 * @example
 *     `dsfr_liste_icones('valeur_de_la_categorie')`
 *     `dsfr_liste_icones('categorie=valeur_de_la_categorie')`
 *     `dsfr_liste_icones('type=valeur_du_type')`
 *     
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `dsfr_liste_icones('liste=categorie')`
 *     `dsfr_liste_icones('liste=type')`
 *  
 * @return array $icones
 * 
 * @see documentation/balises/dsfr_icones
 **/
function dsfr_liste_icones($lister = null) {

	// stockage en $GLOBALS pour éviter de re-charger à chaque fois
	if ( !isset($GLOBALS['dsfr_liste_icones']) ) {
		include_spip('inc/flock');

		$icones = [];

		// on se base sur les class CSS du DSFR pour trouver les chemins car parfois le chemin ne correspond pas au nom de la class CSS
		// ex : `account-fill` utilise le chemin `icons/user/account-circle-fill.svg`
		foreach ( find_all_in_path('lib/dsfr/utility/icons/', '/icons-([a-z]+)\.main\.css$', true) as $chemin ) {
			lire_fichier($chemin, $contenu);
			if ( !empty($contenu) ) {
				preg_match_all('/\.fr-icon-([a-z0-9-]+).*?{(.*?)}/s', $contenu, $correspondances, PREG_SET_ORDER);
				foreach ( $correspondances as $correspondance ) {
					if ( isset($correspondance[1]) && isset($correspondance[2]) ) {
						$class = $correspondance[1];
						$css = $correspondance[2];

						if ( !empty($class) && preg_match('/icons-([a-z]+)\.main\.css$/', $chemin, $categorie) && preg_match('/url\(.*?icons\/(.*?)\.svg/', $css, $url) ) {
							if ( !empty($categorie[1]) && !empty($url[1]) ) {
								list($avant, $apres) = explode('lib/dsfr/utility/icons/', $chemin, 2);
								$icones[$class] = [
									'type'			=> 'dsfr',
									'categorie'		=> $categorie[1],
									'chemin'		=> $avant.'lib/dsfr/icons/'.$url[1].'.svg',
								];
							}
						}
					}
				}
			}
		}

		foreach ( find_all_in_path('dsfr_icones/', '[.]svg$', true) as $chemin ) {
			$filename = pathinfo($chemin, PATHINFO_FILENAME);

			if ( !empty($filename) ) {
				// cas spécial pour certaines icônes systèmes qui commencent par `fr--`
				$class = substr($filename, 0, 4) == 'fr--' ? substr($filename, 4) : $filename;
				list($avant, $apres) = explode('dsfr_icones/', $chemin, 2);
				$categorie = substr($apres, 0, -(strlen('/'.$filename.'.svg')));

				$icones[$class] = [
					'type'			=> 'perso',
					'categorie'		=> $categorie,
					'chemin'		=> $chemin,
				];
			}
		}
	
		$icones = pipeline('configurer_dsfr_liste_icones', $icones);

		$GLOBALS['dsfr_liste_icones'] = $icones;
	}
	else {
		$icones = $GLOBALS['dsfr_liste_icones'];
	}

	return dsfr_composants_lister_tableau($icones, $lister);
}

/**
 * Liste des couleurs de la palette disponibles pour le DSFR
 * 
 * La liste est construite manuellement à partir des fichiers sources de la librairie DSFR :
 *  - `/module/color/variable/_sets.scss`
 *  - `/module/color/variable/_options.scss`
 *  - https://www.systeme-de-design.gouv.fr/elements-d-interface/fondamentaux-de-l-identite-de-l-etat/couleurs-palette
 *  
 * @param string $lister
 *     Voir la fonction `dsfr_composants_lister_tableau()` pour utiliser ce paramètre
 *
 * @example
 *     `dsfr_liste_couleurs('valeur_de_la_categorie')`
 *     `dsfr_liste_couleurs('categorie=valeur_de_la_categorie')`
 *     
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `dsfr_liste_couleurs('liste=categorie')`
 *     `dsfr_liste_couleurs('liste=couleur')`
 * 
 * @return array $couleurs
 * 
 * @see documentation/balises/dsfr_couleurs
 **/
function dsfr_liste_couleurs($lister = null) {

	$couleurs = [
		// couleurs neutres
		'grey' => [
			'titre'			=> 'Gris',
			'categorie'		=> 'neutre',
			'couleur'		=> '#7b7b7b',
		],
		// couleurs primaires
		'blue-france' => [
			'titre'			=> 'Bleu France',
			'categorie'		=> 'primaire',
			'couleur'		=> '#6a6af4',
		],
		'red-marianne' => [
			'titre'			=> 'Rouge Marianne',
			'categorie'		=> 'primaire',
			'couleur'		=> '#e1000f',
		],
		// couleurs système
		'info' => [
			'titre'			=> 'Info',
			'categorie'		=> 'systeme',
			'couleur'		=> '#0078f3',
		],
		'success' => [
			'titre'			=> 'Succès',
			'categorie'		=> 'systeme',
			'couleur'		=> '#1f8d49',
		],
		'warning' => [
			'titre'			=> 'Avertissement',
			'categorie'		=> 'systeme',
			'couleur'		=> '#d64d00',
		],
		'error' => [
			'titre'			=> 'Erreur',
			'categorie'		=> 'systeme',
			'couleur'		=> '#f60700',
		],
		// couleurs illustratives
		'green-tilleul-verveine' => [
			'titre'			=> 'Tilleul verveine',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#B7A73F',
		],
		'green-bourgeon' => [
			'titre'			=> 'Bourgeon',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#68A532',
		],
		'green-emeraude' => [
			'titre'			=> 'Émeraude',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#00A95F',
		],
		'green-menthe' => [
			'titre'			=> 'Menthe',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#009081',
		],
		'green-archipel' => [
			'titre'			=> 'Archipel',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#009099',
		],
		'blue-ecume' => [
			'titre'			=> 'Écume',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#465F9D',
		],
		'blue-cumulus' => [
			'titre'			=> 'Cumulus',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#417DC4',
		],
		'purple-glycine' => [
			'titre'			=> 'Glycine',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#A558A0',
		],
		'pink-macaron' => [
			'titre'			=> 'Macaron',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#E18B76',
		],
		'pink-tuile' => [
			'titre'			=> 'Tuile',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#CE614A',
		],
		'yellow-tournesol' => [
			'titre'			=> 'Tournesol',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#C8AA39',
		],
		'yellow-moutarde' => [
			'titre'			=> 'Moutarde',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#C3992A',
		],
		'orange-terre-battue' => [
			'titre'			=> 'Terre battue',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#E4794A',
		],
		'brown-cafe-creme' => [
			'titre'			=> 'Café Crème',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#D1B781',
		],
		'brown-caramel' => [
			'titre'			=> 'Caramel',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#C08C65',
		],
		'brown-opera' => [
			'titre'			=> 'Opéra',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#BD987A',
		],
		'beige-gris-galet' => [
			'titre'			=> 'Gris galet',
			'categorie'		=> 'illustrative',
			'couleur'		=> '#AEA397',
		],
	];
	
	$couleurs = pipeline('configurer_dsfr_liste_couleurs', $couleurs);

	return dsfr_composants_lister_tableau($couleurs, $lister);
}

/**
 * Fonction qui permet récupérer/définir la valeur d'un cookie DSFR.
 * 
 * En complément de cette fonction, il existe aussi une fonction javascript similaire
 * nommée `DSFR_COOKIE()`. Cette fonction est pré-chargée depuis le squelette
 * `dsfr_composant.js.html` et disponible sur le site public.
 * 
 * @param string $nom_du_cookie
 *     Nom du cookie.
 * 
 * @param string $valeur
 *     Si la valeur est vide ``, alors le cookie sera supprimé.
 * 
 * @param int $nombre_de_jours_avant_suppression
 *     Nombre de jour(s) avant la suppression automatique du cookie.
 *     Par défaut un cookie est supprimé lors de la fermeture du navigateur.
 * 
 * @example
 *     dsfr_cookie('test') // lit le cookie. Renvoi `null` si ce cookie n'existe pas.
 *     dsfr_cookie('test', 'oui') // écrit le cookie avec la valeur `oui`.
 *     dsfr_cookie('test', '') // supprime le cookie.
 * 
 * @return mixed
 **/
function dsfr_cookie($nom_du_cookie, $valeur = null, $nombre_de_jours_avant_suppression = null) {
	$prefixe = 'dsfr_';
	$nom_du_cookie = trim($nom_du_cookie);

	if ( empty($nom_du_cookie) ) {
		return null;
	}

	// modification du cookie
	if ( !is_null($valeur) ) {
		include_spip('inc/cookie');

		if ( !is_null($nombre_de_jours_avant_suppression) ) {
			$nombre_de_jours_avant_suppression = intval($nombre_de_jours_avant_suppression);
		}

		$valeur = trim($valeur);

		$options = [
			'samesite' => 'Lax'
		];
		
		// supprime le cookie
		if ( $valeur === '' ) {
			$options['expires'] = time() - 3600;
		}
		// option d'expiration
		else if ( $nombre_de_jours_avant_suppression > 0 ) {
			$options['expires'] = time() + 3600 * 24 * $nombre_de_jours_avant_suppression;
		}

		$spip_setcookie = spip_setcookie($prefixe.$nom_du_cookie, $valeur, $options);
		if ( $spip_setcookie ) {
			// supprime la valeur de $_COOKIE
			if ( isset($options['expires']) && $options['expires'] < time() ) {
				$valeur = null;
				if ( isset($_COOKIE[$prefixe.$nom_du_cookie]) ) unset($_COOKIE[$prefixe.$nom_du_cookie]);
			}
			else {
				$_COOKIE[$prefixe.$nom_du_cookie] = $valeur;
			}
		}
	}
	// lit le cookie
	else {
		$valeur = isset($_COOKIE[$prefixe.$nom_du_cookie]) ? $_COOKIE[$prefixe.$nom_du_cookie] : null;
	}

	return $valeur;
}

/**
 * Fonction outil pour filter/lister un tableau
 * 
 * @param array $tableau
 *     Tableau à lister.
 * 
 * @param string $lister
 *     Liste demandée en fonction du paramètre.
 *     Si le paramètre ne contient pas le signe `=`, par défaut le paramètre 
 *     suivant s'applique : `$filtre_quoi_par_defaut=$lister`
 *
 * @example
 *     `dsfr_composants_lister_tableau($tableau, 'valeur_de_la_categorie')`
 *     `dsfr_composants_lister_tableau($tableau, 'categorie=valeur_de_la_categorie')`
 *     `dsfr_composants_lister_tableau($tableau, 'quoi=valeur')`
 *     
 *     // Cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `dsfr_composants_lister_tableau($tableau, 'liste=categorie')`
 *     `dsfr_composants_lister_tableau($tableau, 'liste=quoi')`
 **/
function dsfr_composants_lister_tableau($tableau, $lister, $filtre_quoi_par_defaut = 'categorie') {

	// filtre
	if ( !empty($lister) ) {
		$filtre = [];

		if ( strpos($lister, '=') ) {
			list($quoi, $valeur) = explode('=', $lister, 2);

			// cas spécial pour renvoyer uniquement la liste des valeurs
			if ( $quoi == 'liste') {
				$filtre['quoi'] = trim($valeur);

				$liste = [];

				foreach ( $tableau as $cle => $valeur ) {
					if ( array_key_exists($filtre['quoi'], $valeur) ) {
						$filtre['valeur'] = $valeur[$filtre['quoi']];

						if ( !array_key_exists($filtre['valeur'], $liste) ) {
							$liste[$filtre['valeur']] = [];
						}

						$liste[$filtre['valeur']][$cle] = $valeur;
					}
				}

				ksort($liste);
				foreach ( $liste as $cle => $valeur ) {
					ksort($liste[$cle]);
				}

				return $liste;
			}
			else {
				$filtre['quoi'] = trim($quoi);
				$filtre['valeur'] = trim($valeur);
			}
		}
		// filtre par défaut
		else {
			$filtre['quoi'] = trim($filtre_quoi_par_defaut);
			$filtre['valeur'] = trim($lister);
		}

		if ( !empty($filtre) ) {
			if ( $filtre['valeur'] == "''" ) $filtre['valeur'] = ''; // dans le cas ou #VAL{categorie=''}

			foreach ( $tableau as $cle => $valeur ) {
				if ( !array_key_exists($filtre['quoi'], $valeur) || $valeur[$filtre['quoi']] != $filtre['valeur'] ) {
					unset($tableau[$cle]);
				}
			}
		}
	}

	ksort($tableau);

	return $tableau;
}

/**
 * Charge et execute un traitement DSFR depuis le dossier `dsfr_traitements`
 * 
 * @param string $traitement
 *     Nom du traitement
 * 
 * @param array $args
 *     Liste d'arguments à nombre variable pour l'appel de la fonction de traitement
 * 
 * @return mixed
 **/
function dsfr_traitement($traitement, ...$args) {
	$fonction = 'traitement_dsfr_'.$traitement;

	if ( !function_exists($fonction) ) {
		include_spip('dsfr_traitements/'.$traitement);
	}

	$retour = $fonction(...$args);

	return $retour;
}