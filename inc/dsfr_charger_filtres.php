<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Charge tous les filtres disponible(s) pour le DSFR
 * 
 * @return int
 *     Nombre de filtre(s) chargé(s)
 **/
function inc_dsfr_charger_filtres_dist() {
	static $nombre_de_filtres_charges = false;

	if ( $nombre_de_filtres_charges === false ) {
		$nombre_de_filtres_charges = 0;

		foreach ( find_all_in_path('dsfr_filtres/', '[a-z_]+\.php$') as $fichier => $chemin ) {
			if ( find_in_path($fichier, 'dsfr_filtres/', true) ) {
				$nombre_de_filtres_charges++;
			}
		}
	}

	return $nombre_de_filtres_charges;
}