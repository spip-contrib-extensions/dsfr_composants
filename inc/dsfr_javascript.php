<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Retourne un tableau de la liste des fichiers javascript à inclure pour le DSFR.
 * 
 * @return array
 **/
function inc_dsfr_javascript_dist() {
	include_spip('inc/filtres'); // nécessaire pour `produire_fond_statique()`

	// tableau des fichiers javascript
	// clé = uid du fichier, valeur = balise `<script>`
	$javascript = [];

	// librairie DSFR
	$javascript['lib'] = '<script type="module" src="'.timestamp(dsfr('dsfr.module.min.js')).'"></script>';
	$javascript['lib'] .= '<script nomodule src="'.timestamp(dsfr('dsfr.nomodule.min.js')).'"></script>';

	// API analytics du DSFR
	if ( _DSFR_API_ANALYTICS && is_array(_DSFR_API_ANALYTICS) ) {
		$javascript['lib/analytics'] = '<script type="module" src="'.timestamp(dsfr('analytics/analytics.module.min.js')).'"></script>';
		$javascript['lib/analytics'] .= '<script nomodule src="'.timestamp(dsfr('analytics/analytics.nomodule.min.js')).'"></script>';
	}

	// javascript spécifique au plugin
	$javascript['javascript/dsfr'] = '<script src="'.produire_fond_statique('javascript/dsfr.js').'"></script>';

	// pipeline permettant d'inclure/modifier les fichiers javascript
	$javascript = pipeline('dsfr_javascript', $javascript);

	return $javascript;
}