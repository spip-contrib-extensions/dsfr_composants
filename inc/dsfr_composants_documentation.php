<?php
/**
 * Fonctions du plugin chargées par SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Documentation du plugin
 * 
 * @param string $documentation
 *     Chemin de la documentation demandée
 * 
 * @example
 *     `dsfr_composants_documentation()`
 *     `dsfr_composants_documentation('composants')`
 *     `dsfr_composants_documentation('balises/dsfr_composants')`
 * 
 * @return mixed
 **/
function dsfr_composants_documentation($documentation = null) {

	$dossier_du_plugin = dirname(dirname(__FILE__));

	// cas spécial pour le README.md et CHANGELOG.md
	if ( $documentation == 'readme' || $documentation == 'changelog' || $documentation == 'license' ) {
		include_spip('inc/flock');

		$chemin = _DIR_PLUGIN_DSFR_COMPOSANTS.strtoupper($documentation).'.md';

		lire_fichier($chemin, $contenu);

		return !empty($contenu) ? trim($contenu) : false;
	}

	$structure = [
		'balises'		=> [
			'titre'			=> 'Balises',
			'description'	=> '
				Le plugin permet l\'utilisation de plusieurs balises SPIP spécifiques.
			',
			'explication'	=> '
				Vous pouvez retrouver toutes ces balises dans le dossier <code>balise/</code> du plugin.
			',
		],
		'composants'	=> [
			'titre'			=> 'Composants DSFR',
			'description'	=> '
				Le plugin fournit plusieurs squelettes SPIP sous forme de composants DSFR.
			',
			'explication'	=> '
				Tous les squelettes de composants DSFR sont contenus dans le dossier <code>dsfr_composants/</code>.
				<br>
				Vous pouvez ajouter vos propres squelettes de composants DSFR dans le dossier <code>dsfr_composants/</code>
				(en tant que sous-dossier dans le dossier de votre plugin, ou dans votre dossier SPIP <code>squelettes/dsfr_composants/</code>).
				<br><br>
				Vous pouvez inclure les composants DSFR avec la syntaxe d\'inclusion de SPIP <code>&lt;INCLURE&gt;</code> ou <code>#INCLURE</code> (en statique).
			',
		],
		'dossiers'		=> [
			'titre'			=> 'Dossiers',
			'description'	=> '
				Le plugin utilise des dossiers spécifiques que vous pouvez surcharger en fonction de vos besoins.
			',
			'explication'	=> '
				Pour effectuer une surcharge, vous pouvez ajouter vos propres dossiers en tant que sous-dossiers 
				dans le dossier de votre propre plugin, ou dans votre dossier SPIP <code>squelettes/</code>.
			',
		],
		'filtres'		=> [
			'titre'			=> 'Filtres',
			'description'	=> '
				Le plugin permet l\'utilisation de plusieurs filtres SPIP spécifiques.
			',
			'explication'	=> '
				Tous les filtres DSFR sont contenus dans le dossier <code>dsfr_filtres/</code>.
				<br>
				Vous pouvez ajouter vos propres filtres DSFR dans le dossier <code>dsfr_filtres/</code>
				(en tant que sous-dossier dans le dossier de votre plugin, ou dans votre dossier SPIP <code>squelettes/dsfr_filtres/</code>).
			',
		],
		'fonctions'		=> [
			'titre'			=> 'Fonctions PHP',
			'description'	=> '
				Le plugin propose des fonctions PHP spécifiques que vous pouvez utiliser dans votre code PHP ou en tant que pipeline dans vos squelettes SPIP.
			',
			'explication'		=> '
				Vous pouvez retrouver toutes ces fonctions dans le fichier <code>inc/dsfr_composants.php</code> du plugin.
			',
		],
		'pipelines'		=> [
			'titre'			=> 'Pipelines',
			'description'	=> '
				Pipelines SPIP spécifiques permettant de modifier dynamiquement certaines fonctionnalitées du plugin.
			',
		],
	];

	// aucun chemin précisé, on renvoie uniquement la structure
	if ( empty($documentation) ) {
		return $structure;
	}

	// un type de documentation est précisé
	if ( array_key_exists($documentation,$structure) ) {
		$type = $documentation;

		$documentation = $structure[$type];
		// charge la liste de toute la documentation du type demandé
		$documentation['liste'] = dsfr_composants_documentation_liste($type);

		return $documentation;
	}

	return false;
}

/**
 * Charge la liste de toute la documentation d'un certain type
 * 
 * @param string $type
 *     Type de documentation demandée
 * 
 * @example
 *     `dsfr_composants_documentation_liste('balises')`
 *     `dsfr_composants_documentation_liste('composants')`
 * 
 * @return array
 **/
function dsfr_composants_documentation_liste($type) {
	$dossier_du_plugin = dirname(dirname(__FILE__));

	$liste = [];

	/*
		Balises SPIP
	*/
	if ( $type == 'balises' ) {
		foreach ( find_all_in_path($dossier_du_plugin.'/balise/', '[.]php$') as $chemin ) {
			$balise = basename($chemin, '.php');

			$fonction = 'balise_'.strtoupper($balise).'_dist';
			if ( !function_exists($fonction) ) { include $chemin; } // charge la balise si elle n'a pas été encore utilisée

			// uniquement si la fonction existe
			if ( function_exists($fonction) ) {
				$reflector = new ReflectionFunction($fonction);
				if ( $commentaire = $reflector->getDocComment() ) {
					$commentaire = preg_replace('#^\\s*\\* ?#m', '', trim($commentaire, "/* \r\n\t")); // supprime les /** * */
				}

				$liste[$balise] = [
					'titre'					=> '#'.strtoupper($balise),
					'chemin'				=> $chemin,
					'description'			=> $commentaire,
					'description-language'	=> 'phpdoc',
				];
			}
		}
	}
	/*
		Composants DSFR
	*/
	else if ( $type == 'composants' ) {
		include_spip('inc/flock');

		foreach ( dsfr_liste_composants() as $composant => $chemin ) {
			$description = false;

			lire_fichier($chemin, $contenu);
			if ( !empty($contenu) ) {
				preg_match_all('/\[(([^\[\]]*|(?R))*)\]/', $contenu, $correspondances, PREG_SET_ORDER);
				foreach ( $correspondances as $correspondance ) {
					if ( isset($correspondance[1]) && substr($correspondance[1], 0, 5) == '(#REM' ) {
						$description = trim(preg_replace('/\(#REM(\s+|)\)/','',$correspondance[1]));
						break;
					}
				}
			}

			$liste[$composant] = [
				'titre'					=> $composant,
				'chemin'				=> $chemin,
				'description'			=> $description,
				'description-language'	=> 'phpdoc',
			];

			if ( $demo = find_in_path('dsfr_composants/'.$composant.'.demo.html') ) {
				$liste[$composant]['demo'] = $demo;
			}
		}
	}
	/*
		Dossiers
	*/
	else if ( $type == 'dossiers' ) {
		include_spip('inc/flock');

		foreach ( glob($dossier_du_plugin.'/dsfr_*' , GLOB_ONLYDIR) as $chemin ) {
			$dossier = basename($chemin);

			lire_fichier($chemin.'/README.md', $contenu);

			$liste[$dossier] = [
				'titre'					=> $dossier.'/',
				'chemin'				=> $dossier_du_plugin.'/'.$dossier,
				'description'			=> !empty($contenu) ? trim($contenu) : false,
				'description-language'	=> 'markdown',
			];
		}
	}
	/*
		Filtres SPIP
	*/
	else if ( $type == 'filtres' ) {
		foreach ( find_all_in_path($dossier_du_plugin.'/dsfr_filtres/', '[.]php$') as $chemin ) {
			$filtre = 'dsfr_'.basename($chemin, '.php');

			$fonction = 'filtre_'.$filtre.'_dist';
			if ( !function_exists($fonction) ) { include $chemin; } // charge la balise si elle n'a pas été encore utilisée

			// uniquement si la fonction existe
			if ( function_exists($fonction) ) {
				$reflector = new ReflectionFunction($fonction);
				if ( $commentaire = $reflector->getDocComment() ) {
					$commentaire = preg_replace('#^\\s*\\* ?#m', '', trim($commentaire, "/* \r\n\t")); // supprime les /** * */
				}

				$liste[$filtre] = [
					'titre'					=> '|'.$filtre,
					'chemin'				=> $chemin,
					'description'			=> $commentaire,
					'description-language'	=> 'phpdoc',
				];
			}
		}
	}
	/*
		Fonctions du plugin
	*/
	else if ( $type == 'fonctions' ) {
		$chemin = $dossier_du_plugin.'/inc/dsfr_composants.php';

		if ( $lignes = file($chemin) ) {
			foreach ( $lignes as $ligne ) {
				$ligne = trim($ligne);
				if ( substr($ligne, 0, 8) == 'function' ) {
					$fonction = substr($ligne, 9, strpos($ligne, '(') - 9);
	
					// uniquement si la fonction existe
					if ( function_exists($fonction) ) {
						$reflector = new ReflectionFunction($fonction);
						if ( $commentaire = $reflector->getDocComment() ) {
							$commentaire = preg_replace('#^\\s*\\* ?#m', '', trim($commentaire, "/* \r\n\t")); // supprime les /** * */
						}

						$liste[$fonction] = [
							'titre'					=> $fonction.'()',
							'description'			=> $commentaire,
							'description-language'	=> 'phpdoc',
						];
					}
				}
			}
		}
	}
	/*
		Pipelines SPIP
	*/
	else if ( $type == 'pipelines' ) {
		include_spip('inc/filtres');
		$info_plugin = charger_filtre('info_plugin');
	
		$dsfr_composants = $info_plugin('dsfr_composants', 'tout');
		if ( isset($dsfr_composants['pipeline']) && is_array($dsfr_composants['pipeline']) ) {
			foreach ( $dsfr_composants['pipeline'] as $pipeline ) {
				// uniquement les pipelines spécifiques au plugin
				if ( isset($pipeline['action']) && empty($pipeline['action']) ) {
					$pipeline = $pipeline['nom'];

					$liste[$pipeline] = [
						'titre'			=> $pipeline,
					];
				}
			}
		}
	}

	// cherche les documentations associées avec le tag `@see documentation/(.*)` dans la description
	foreach ( $liste as $cle => $valeur ) {
		if ( !empty($valeur['description']) ) {
			
			preg_match_all('/@([a-z]+?)\s+([^\n]*)/i', $valeur['description'], $tags, PREG_SET_ORDER);
			foreach ( $tags as $tag ) {
				// documentation associée
				if ( $tag[1] == 'see' && substr($tag[2], 0, 14) == 'documentation/' ) {
					$da = [];
					$da_source = trim(substr($tag[2], 14));
					$da_infos = array_values(array_filter(array_map('trim',explode('/', $da_source, 2))));
					if ( !empty($da_infos) ) {
						$da['type'] = $da_infos[0];

						if ( isset($da_infos[1]) ) {
							$da['documentation'] = $da_infos[1];
						}
					}

					if ( !empty($da) ) {
						$liste[$cle]['documentation_associee'][$da_source] = $da;
					}
				}
			}
		}
	}

	ksort($liste);

	return $liste;
}