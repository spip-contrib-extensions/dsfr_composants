<?php
/**
 * Fonctions du plugin surchargeables
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Mise en conformité DSFR du contenu de la balise `<head>`
 * 
 * @param string $contenu_balise_head
 * 
 * @return string
 **/
function inc_dsfr_head_dist($contenu_balise_head) {
	// nécessaire pour `timestamp()` et `produire_fond_statique()`
	include_spip('inc/filtres');

	// supprimer les espaces superflus
	$contenu_balise_head = trim(preg_replace('/\s*<(meta|script|link|style|title|!--)/i', "\n\t".'<$1', $contenu_balise_head));

	// ajoute la configuration de l'API du DSFR
	if ( strpos($contenu_balise_head, 'window.dsfr') === false ) {
		$dsfr_api = [
			'mode'			=> 'auto',
			'verbose'		=> _DSFR_API_JS_VERBOSE ? true : false,
			'level'			=> 'info',
			'production'	=> _DSFR_API_JS_PRODUCTION ? true : false,
		];
	
		if ( _DSFR_API_JS_MODE && in_array(_DSFR_API_JS_MODE, ['auto','manual','runtime','loaded']) ) {
			$dsfr_api['mode'] = _DSFR_API_JS_MODE;
		}
		if ( _DSFR_API_JS_LEVEL && in_array(_DSFR_API_JS_LEVEL, ['log','debug','info','warn','error']) ) {
			$dsfr_api['level'] = _DSFR_API_JS_LEVEL;
		}
		if ( _DSFR_API_ANALYTICS && is_array(_DSFR_API_ANALYTICS) ) {
			$dsfr_api['analytics'] = _DSFR_API_ANALYTICS;
		}

		// ajout au début pour pouvoir être utilisé dans tous les fichiers javascripts (dont ceux chargés dans la balise `<head>`)
		$contenu_balise_head = '<!-- dsfr_api -->'."\n\t".'<script>window.dsfr='.json_encode($dsfr_api, 1).'</script>'."\n\t".$contenu_balise_head;
	}

	// ajoute la meta `charset` (attention tester `<meta charset="[...]">` et `<meta http-equiv="Content-Type" content="text/html; charset=[...]">`)
	if ( !preg_match('/<meta[^>]*charset=/i', $contenu_balise_head) ) {
		include_spip('inc/config');
		// ajout au début pour validité W3C
		$contenu_balise_head = '<meta charset="'.lire_config('charset').'">'."\n\t".$contenu_balise_head;
	}

	$ajouter_a_la_balise_head = '';
	// ajouter la meta `viewport`
	if ( !preg_match('/<meta[^>]*name=["\']viewport["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no">';
	}
	// ajouter la meta `format-detection`
	if ( !preg_match('/<meta[^>]*name=["\']format-detection["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<meta name="format-detection" content="telephone=no,date=no,address=no,email=no,url=no">';
	}
	// ajouter la meta `theme-color`
	if ( !preg_match('/<meta[^>]*name=["\']theme-color["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<meta name="theme-color" content="#000091">';
	}
	// ajouter le favicon
	if ( !preg_match('/<link[^>]*rel=["\']apple-touch-icon["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<link rel="apple-touch-icon" href="'.timestamp(dsfr('favicon/apple-touch-icon.png')).'">';
	}
	if ( !preg_match('/<link[^>]*rel=["\']icon["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<link rel="icon" href="'.timestamp(dsfr('favicon/favicon.svg')).'" type="image/svg+xml">';
	}
	if ( !preg_match('/<link[^>]*rel=["\']shortcut icon["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<link rel="shortcut icon" href="'.timestamp(dsfr('favicon/favicon.ico')).'" type="image/x-icon">';
	}
	// ajouter le manifest
	if ( !preg_match('/<link[^>]*rel=["\']manifest["\']/i', $contenu_balise_head) ) {
		$ajouter_a_la_balise_head .= "\n\t".'<link rel="manifest" href="'.timestamp(dsfr('favicon/manifest.webmanifest')).'" crossorigin="use-credentials">';
	}

	if ( !empty($ajouter_a_la_balise_head) ) {
		$contenu_balise_head .= "\n\t".'<!-- dsfr_head -->'.$ajouter_a_la_balise_head;
	}

	return trim($contenu_balise_head);
}