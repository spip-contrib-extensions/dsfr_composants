<?php
/**
 * Fonctions communes du plugin spécifiques au DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Charge tous les filtres disponible(s) pour le DSFR.
 * 
 * Appel de la fonction surchargeable `inc_dsfr_charger_filtres_dist()`.
 * 
 * @return int
 *     Nombre de filtre(s) chargé(s)
 **/
function dsfr_charger_filtres() {
	static $nombre_de_filtres_charges;

	if ( is_null($nombre_de_filtres_charges) ) {
		$f = charger_fonction('dsfr_charger_filtres', 'inc');
		$nombre_de_filtres_charges = $f();
	}
	
	return $nombre_de_filtres_charges;
}

/**
 * Enregistrement des événements DSFR
 * 
 * @param string $message
 *     Message à loger
 * @param int $niveau
 *     Niveau de log, tel que `_LOG_DEBUG`
 **/
function dsfr_log($message = null, $niveau = null) {
	spip_log($message, 'dsfr'.($niveau ? '.'.$niveau : ''));
}

/**
 * Déclenche le traitement d'un modèle DSFR
 * 
 * Cette fonction est appelée depuis le squelette `/modeles/dsfr.html`
 * 
 * @param array $env
 * 
 * @return string
 *     Résultat du traitement
 **/
function dsfr_modele($env = []) {
	$nom = '';
	$raccourci = false; // est-ce que l'inclusion provient de l'utilisation d'un raccourci `<dsfr|...>` ?
	$data = '';

	$env = (is_string($env) ? @unserialize($env) : $env);

	// non utilisé par les modèles DSFR
	unset($env['id']);
	unset($env['id_dsfr']);

	// appelé depuis un raccourci de modèle `<dsfr|...>` passé par `propre()`
	if ( array_key_exists('args', $env) && is_array($env['args']) ) {
		$raccourci = true;

		// trim et supprime les paramètres vides mais garde les `0`
		$parametres_raccourci = array_filter(array_map('trim', $env['args']),'strlen');
		unset($env['args']);

		// fonctionnement SPIP pour le 2ème argument du modèle
		if ( array_key_exists('class', $env) ) {
			$nom = $env['class'];
			unset($env['class']);

			// fix qui dans certain cas supprime la class/nom ajouté aussi à `args`
			// le nom du modele est aussi présent dans les paramètres du raccourci 
			if ( array_key_exists($nom, $parametres_raccourci) && $parametres_raccourci[$nom] === $nom ) {
				unset($parametres_raccourci[$nom]);
			}
			else if ( $cle = array_key_first($parametres_raccourci) ) {
				// le nom du modèle est en MAJ présent dans les paramètres du raccourci 
				if ( strtolower($cle) == strtolower($nom) && $parametres_raccourci[$cle] === $cle ) {
					unset($parametres_raccourci[$cle]);
				}
			}
		}

		// essaye de prendre la clé du premier paramètre (ex. `<dsfr|composant=alerte|...>`)
		if ( empty($nom) ) {
			$nom = array_key_first($parametres_raccourci);			
		}

		// écrase le contexte d'environnement avec les paramètres du raccourcis normalement plus propre
		$env = array_merge($env, $parametres_raccourci);

		// ajoute les paramètres du raccourcis pour pouvoir travailler avec si besoin et passe toutes les clés en minuscule pour éviter les erreurs de frappes
		$env['parametres_raccourci'] = array_change_key_case($parametres_raccourci, CASE_LOWER);
	}
	// appelé depuis la balise `#MODELE`
	else if ( array_key_exists('nom', $env) ) {
		$nom = $env['nom'];
		unset($env['nom']);
	}

	if ( $fonction_dsfr_modeles_traiter_modele = charger_fonction('traiter_modele', 'dsfr/modeles') ) {
		$data = $fonction_dsfr_modeles_traiter_modele($nom, $env, $raccourci);
	}

	//$data .= spip_balisage_code(var_export($env,1),1,'','php'); // debug ;)

	return $data;
}