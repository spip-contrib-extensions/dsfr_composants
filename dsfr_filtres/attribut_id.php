<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_attribut_id`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Transforme un texte en `id` pouvant être utilisé
 * en toute sécurité pour l'attribut `id` d'une balise HTML.
 * 
 * Le texte est nettoyé : translitère et remplace tous les caractères spéciaux.
 *
 * @example
 *     `<balise[ id="(#BALISE**|dsfr_attribut_id)"]></balise>`
 * 
 * @param ?string $texte
 *     Texte à transformer en `id` HTML
 * @param string $remplacer_les_caracteres_speciaux_par
 *     Texte de substitution des caractères spéciaux trouvés
 * 
 * @return string
 *     Texte prêt pour être utilisé pour l'attribut `id` d'une balise HTML
 **/
function filtre_dsfr_attribut_id_dist(?string $texte, string $remplacer_les_caracteres_speciaux_par = '_'): string {
	if ( $texte === null || $texte === '' ) {
		return '';
	}

	// pas de balise html
	if ( strpos($texte, '<') !== false ) {
		$texte = strip_tags($texte);
	}

	if ( preg_match('/[^-_a-zA-Z0-9]+/S', $texte) ) {
		if ( !function_exists('translitteration') ) {
			include_spip('inc/charsets');
		}
		if ( strpos($texte, '&') !== false ) {
			$texte = unicode2charset($texte);
		}

		// enlève les accents et cie
		$texte = translitteration($texte);
	}

	// supprime les espaces indésirables
	$texte = trim($texte);

	// remplace tout les caractères spéciaux
	$texte = preg_replace('/[^-_a-zA-Z0-9]/', $remplacer_les_caracteres_speciaux_par, $texte);

	// nettoyer les doubles occurences du texte de substitution des caractères spéciaux
	if ( $remplacer_les_caracteres_speciaux_par !== '' ) {
		$texte = preg_replace('/'.$remplacer_les_caracteres_speciaux_par.'+/', $remplacer_les_caracteres_speciaux_par, $texte);

		// pas de texte de substitution au debut ou à la fin
		$texte = trim($texte, $remplacer_les_caracteres_speciaux_par);
	}

	return (string) $texte;
}