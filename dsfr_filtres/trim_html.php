<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_trim_html`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Supprime les espacements HTML suivants d'un texte :
 * - les `<p></p>` vides
 * - les `<br>` au début et à la fin
 * - les espaces insécables `&nbsp;` au début et à la fin
 * - les espaces et sauts de ligne ` \n\r\t\v\x00` au début et à la fin
 * 
 * @param ?string $texte
 *     Texte à traiter
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function filtre_dsfr_trim_html_dist(?string $texte): string {
	if ( $texte === null || $texte === '' ) {
		return '';
	}

	// supprime les `<p></p>` vides
	$texte = preg_replace('/<p>\s*<\/p>/i', '', $texte);

	// supprime les `<br>` au début et à la fin (en prenant compte des éventuels `&nbsp;`)
	$texte = preg_replace('/^((\s|&nbsp;)*<br\s*\/?>(\s|&nbsp;)*)+|((\s|&nbsp;)*<br\s*\/?>(\s|&nbsp;)*)+$/i', '', $texte);

	// supprime les espaces insécables `&nbsp;` au début et à la fin
	$texte = preg_replace('/^(\s*&nbsp;\s*)+|(\s*&nbsp;\s*)+$/i', '', $texte);

	// supprime les espaces et sauts de ligne `\n\r\t\v\x00` au début et à la fin
	$texte = trim($texte);

	return $texte;
}