<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_ajouter_apres{valeur,valeur à ajouter}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Ajoute un ou plusieurs éléments après une valeur spécifique.
 * À noter que les valeurs vides ne sont pas ajoutées !
 * Dans le cas ou la valeur concernée n'éxiste pas, l'ajout est effectué à la fin du tableau 
 * 
 * @example
 *     `#SET{tableau,#LISTE{camembert,morbier,mimolette}}`
 *     `#SET{tableau,#GET{tableau}|dsfr_tableau_ajouter_apres{morbier,brie}}`
 * 
 * @param array $tableau
 * @param mixed $valeur
 * @param mixed ...$valeurs_a_ajouter
 * 
 * @return array
 **/
function filtre_dsfr_tableau_ajouter_apres_dist(array $tableau, mixed $valeur): array {

	$position = array_search($valeur, $tableau);

	$arguments = func_get_args();

	// supprime les 2 premiers arguments qui concernent le tableau et la valeur
	array_shift($arguments);
	array_shift($arguments);

	foreach ( $arguments AS $valeur_a_ajouter ) {
		if ( !empty($valeur_a_ajouter) ) {
			if ( $position !== false ) {
				$position++;

				array_splice($tableau, $position, 0, $valeur_a_ajouter);
			}
			else {
				array_push($tableau, $valeur_a_ajouter);
			}
		}
	}

	return (array) $tableau;
}