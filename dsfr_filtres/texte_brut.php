<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_texte_brut`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Version plus agressive du filtre SPIP `textebrut` qui supprime
 * aussi les espaces en doubles et tous les sauts de ligne pour renvoyer 
 * une chaine de charactère sur une seule ligne (sans "\n" ou "\r").
 * 
 * @param ?string $texte
 *     Texte à traiter
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function filtre_dsfr_texte_brut_dist(?string $texte): string {
	if ( $texte === null || $texte === '' ) {
		return '';
	}

	// filtre SPIP
	$texte = filtrer('textebrut', $texte);

	// supprimer tous les sauts de ligne
	$texte = str_replace(array("\r", "\n"), ' ', $texte);

	// supprimer les espaces en doubles
	$texte = preg_replace('/\s+/', ' ', $texte);

	// supprime les espaces restant au début et à la fin
	$texte = trim($texte);

	return $texte;
}