<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_ajouter{valeur}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Ajoute un ou plusieurs éléments à la fin d'un tableau.
 * À la différence de `array_push`, les valeurs vides ne sont pas ajoutées !
 * 
 * @example
 *     `#ARRAY|dsfr_tableau_ajouter{camembert}`
 *     `#ARRAY|dsfr_tableau_ajouter{camembert,morbier,mimolette}`
 *     `#ARRAY|dsfr_tableau_ajouter{#LISTE{camembert,morbier,mimolette}}`
 * 
 * @param array $tableau
 * @param mixed ...$valeurs_a_ajouter
 * 
 * @return array
 **/
function filtre_dsfr_tableau_ajouter_dist(array $tableau): array {

	$arguments = func_get_args();

	// supprime le 1er argument qui concerne le tableau
	array_shift($arguments);

	foreach ( $arguments AS $valeur_a_ajouter ) {
		if ( !empty($valeur_a_ajouter) ) {
			array_push($tableau, $valeur_a_ajouter);
		}
	}

	return (array) $tableau;
}