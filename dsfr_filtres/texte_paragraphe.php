<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_texte_paragraphe{balises_a_conserver}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Transforme un texte pour pouvoir l'insérer dans un paragraphe `<p>`.
 * - transforme les titres `<h[123456]>titre</h[123456]>` en `<strong>titre</strong><br>`
 * - supprime toutes les balises html du texte sauf `<a><span><strong><em><i><del><abbr><sup><sub><code><p>`
 * - transforme les `<p>` en saut de ligne `<br>`
 * 
 * @param ?string $texte
 *     Texte à traiter
 * 
 * @param string $balises_a_conserver
 *     Liste des balises à concerver séparées par le symbole `|` (le pipe, un trait vertical)
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function filtre_dsfr_texte_paragraphe_dist(?string $texte, string $balises_a_conserver = 'a|span|strong|em|i|del|abbr|sup|sub|code'): string {
	if ( $texte === null || $texte === '' ) {
		return '';
	}

	// prepare les balises à conserver (sans doublon et non vide...)
	$balises_a_conserver = explode('|',trim($balises_a_conserver));
	$balises_a_conserver = array_map('trim', $balises_a_conserver);
	$balises_a_conserver = array_filter($balises_a_conserver);

	// supprime les `<p>` vides
	$texte = preg_replace('/<p[^>]*>(?:\s|&nbsp;)*<\/p>/i', '', $texte);

	// transforme les titres `<h[123456]>titre</h[123456]>` en `<strong>titre</strong><br>`
	if ( str_contains($texte, '<h') && preg_match_all('/<h([123456])(?:<[^<>]*>|[^>])*>(.*)<\/h\\1>/UimsS', $texte, $titres_trouves, PREG_SET_ORDER) ) {
		/*
			- index 0 : la capture de l'intertitre complet
			- index 1 : le niveau de l'intertire (1,2,3,4,5 ou 6)
			- index 2 : le contenu du texte compris entre les balises de l'intertitre
		*/
		foreach ( $titres_trouves as $titre ) {
			$titre_position = strpos($texte, $titre[0]);
			$titre_longueur = strlen($titre[0]);
			$titre_contenu = trim($titre[2]);
			if ( !empty($titre_contenu) ) {
				$titre_contenu = '<strong>'.$titre_contenu.'</strong><br>';
			}

			$texte = substr_replace($texte,  $titre_contenu, $titre_position, $titre_longueur);
		}
	}

	// ajoute un espace pour éviter de transformer `<li>texte</li><li>texte</li>` en `textetexte`
	$texte = preg_replace('/><(\w+)/US', '> <$1', $texte);

	// ajoute `p` et `br` dans les balises à conserver
	$balises_a_conserver[] = 'p';
	$balises_a_conserver[] = 'br';
	$balises_a_conserver = array_unique($balises_a_conserver);

	// supprime les balises sauf celles à concerver (conserve aussi les commentaires HTML)
	$texte = preg_replace('/<(?!\/?('.implode('|',$balises_a_conserver).'))(!--|\w|\/|!\[endif|!\[if)(?:<[^<>]*>|[^>])*>/UimsS', '', $texte);
	// ne pas oublier une balise non fermée à la fin car coupée
	$texte = preg_replace('/<(!--|\w|\/)[^>]*$/US', '', $texte);

	// transforme des `<p>` en double saut de ligne `<br>` (simule visuellement un paragraphe)
	// l'utilisation de `PtoBR($texte)` ne convenait pas car elle ne donne pas le même effet en remplaçant les paragraphes par 1 seul saut de ligne
	$texte = preg_replace('/<p[^>]*?>/', '', $texte);
	$texte = str_replace('</p>', '<br><br>', $texte);  // double saut de ligne `<br>`

	// supprime les espaces en doubles
	$texte = preg_replace('/\s+/', ' ', $texte);

	// texte transformé en supprimant les espaces malvenus
	return filtrer('dsfr_trim_html', $texte);
}