<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_env_supprimer`
 * 
 * @author		Jonathan OCHEJ
 * @author		placido, voir https://discuter.spip.net/t/resolu-inverse-du-filtre-setenv-par-ex-delenv/181287
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Filtre qui supprime une ou plusieurs valeurs de l'environnement du squelette
 * Fonctionnement inverse du filtre `|setenv` https://www.spip.net/fr_article5840.html
 *
 * @example
 *     `[(#VAL{nom_valeur_a_supprimer}|dsfr_env_supprimer)]`
 * 
 *     `[(#VAL{nom_valeur_a_supprimer autre_nom_valeur_a_supprimer}|dsfr_env_supprimer)]`
 *     `[(#LISTE{nom_valeur_a_supprimer,autre_nom_valeur_a_supprimer}|dsfr_env_supprimer)]`
 * 
 * @param array $Pile
 * @param string|array $nom(s)
 * 
 * @return string ''
 **/
function filtre_dsfr_env_supprimer_dist(&$Pile, mixed $nom) {
	if ( !isset($Pile[0]) || !is_array($Pile[0]) ) {
		return '';
	}

	$noms_a_supprimer = [];

	if ( is_string($nom) ) {
		$noms_a_supprimer = explode(' ',trim($nom));
	}
	else if ( is_int($nom) ) {
		$noms_a_supprimer[] = $nom;
	}
	else if ( is_array($nom) ) {
		$noms_a_supprimer = $nom;
	}

	foreach ( $noms_a_supprimer AS $n ) {
		if ( is_string($n) || is_int($n) ) {
			if ( array_key_exists($nom, $Pile[0]) ) {
				unset($Pile[0][$nom]);
			}
		}
	}

	return '';
}