<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_env_definir`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Filtre qui définit la `valeur` de l'environnement du squelette.
 *
 * @example
 *     `[(#ARRAY{cle1,valeur1,cle2,valeur2...}|dsfr_env_definir)]`
 * 
 * @param array $Pile
 * @param array $contexte
 * @param string|array $preserver
 * 
 * @return string ''
 **/
function filtre_dsfr_env_definir_dist(&$Pile, $contexte, $preserver = 'spip_env') {
	if ( !is_array($contexte) ) {
		$contexte = [];
	}

	// contexte à préserver
	$contexte_a_preserver = [];
	if ( $preserver ) {
		if ( is_string($preserver) ) {
			$contexte_a_preserver = array_unique(array_filter(array_map('trim', explode(' ',trim($preserver)))));
		}
		else if ( is_int($preserver) ) {
			$contexte_a_preserver[] = $preserver;
		}
		else if ( is_array($preserver) ) {
			$contexte_a_preserver = $preserver;
		}
	}
	// préserver le contexte SPIP
	if ( ($cle = array_search('spip_env', $contexte_a_preserver)) !== false) {
		unset($contexte_a_preserver[$cle]);

		$contexte_a_preserver = array_merge($contexte_a_preserver,[
			'lang','date','date_default','date_redac','date_redac_default','type-page','debut_?'
		]);
	}

	// sauvegarder si possible les valeurs du contexte à préserver
	$sauvegarde = [];
	if ( isset($Pile[0]) && is_array($Pile[0]) ) {
		foreach ( $contexte_a_preserver AS $cle ) {
			// préserver toutes les clés qui commence par
			if ( is_string($cle) && str_ends_with($cle,'?') ) {
				$cle = trim(substr($cle, 0, -1));
				if ( !empty($cle) ) {
					foreach ( $Pile[0] AS $c => $v ) {
						if ( is_string($c) && str_starts_with($c, $cle) ) {
							$sauvegarde[$c] = $v;
						}
					}
				}
			}
			else if ( is_string($cle) || is_int($cle) ) {
				if ( array_key_exists($cle, $Pile[0]) ) {
					$sauvegarde[$cle] = $Pile[0][$cle];
				}
			}
		}
	}

	$Pile[0] = array_merge($contexte,$sauvegarde);

	return '';
}