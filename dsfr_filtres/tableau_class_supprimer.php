<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_class_supprimer{valeur}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Supprime une ou plusieurs classes CSS dans un tableau.
 * 
 * @example
 *     `#ARRAY|dsfr_tableau_class_supprimer{nom_de_classe_css}`
 * 
 *     `#SET{attribut_class,#ARRAY}`
 *     `#SET{attribut_class,#GET{attribut_class}|dsfr_tableau_class_ajouter{nom_de_classe_css}}`
 *     `#SET{attribut_class,#GET{attribut_class}|dsfr_tableau_class_supprimer{nom_de_classe_css}}`
 * 
 *     `#ARRAY|dsfr_tableau_class_supprimer{nom_de_classe_css autre_nom_de_classe_css}`
 *     `#ARRAY|dsfr_tableau_class_supprimer{#LISTE{nom_de_classe_css,autre_nom_de_classe_css}}`
 * 
 * @param array $tableau_de_classes_css
 * @param string|array $valeur
 * 
 * @return array
 **/
function filtre_dsfr_tableau_class_supprimer_dist(array $tableau_de_classes_css, $valeur): array {

	if ( is_string($valeur) ) {
		$valeur = explode(' ',trim($valeur));
	}

	if ( is_array($valeur) ) {
		$valeur = array_map('trim', $valeur);
		$valeur = array_filter($valeur);
		$valeur = array_unique($valeur);
	}

	if ( !is_array($valeur) || empty($valeur) ) {
		return $tableau_de_classes_css;
	}

	return array_values(array_diff($tableau_de_classes_css, $valeur));
}