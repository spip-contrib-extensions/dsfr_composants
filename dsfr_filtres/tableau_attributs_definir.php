<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_attributs_definir{nom,valeur}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Définit la `valeur` d'un attribut `nom` dans un tableau.
 * 
 * Si l'attribut `nom` existe déjà dans le tableau, sa `valeur` est modifiée par la nouvelle.
 * Si `nom` ou `valeur` est vide, le tableau n'est pas modifié.
 * Si `valeur=booleen` alors l'attribut sera considéré comme un booléen lors de l'appel du filtre. `|dsfr_tableau_attributs_html`
 * 
 * @example
 *     `#ARRAY|dsfr_tableau_attributs_definir{nom_attribut_html,valeur}`
 *     `#ARRAY|dsfr_tableau_attributs_definir{nom_attribut_html,booleen}`
 * 
 *     `#SET{attributs_html,#ARRAY}`
 *     `#SET{attributs_html,#GET{attributs_html}|dsfr_tableau_attributs_definir{nom_attribut_html,valeur}}`
 * 
 * @param array $tableau_attributs
 * @param string $nom
 * @param string $valeur
 * 
 * @return array
 **/
function filtre_dsfr_tableau_attributs_definir_dist(array $tableau_attributs, string $nom, ?string $valeur): array {

	if ( $valeur !== null && $valeur !== '' ) {
		$nom = trim($nom);
		$valeur = trim((string) $valeur);

		if ( $nom !== '' && $valeur !== '' ) {
			$tableau_attributs[$nom] = $valeur;
		}
	}

	return (array) $tableau_attributs;
}