<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_attributs_supprimer{nom}`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Supprime l'attribut `nom` du tableau.
 * 
 * @example
 *     `#ARRAY|dsfr_tableau_attributs_supprimer{nom_attribut_html}`
 * 
 *     `#SET{attributs_html,#ARRAY}`
 *     `#SET{attributs_html,#GET{attributs_html}|dsfr_tableau_attributs_definir{nom_attribut_html,valeur}}`
 *     `#SET{attributs_html,#GET{attributs_html}|dsfr_tableau_attributs_supprimer{nom_attribut_html}}`
 * 
 * @param array $tableau_attributs
 * @param string $nom
 * 
 * @return array
 **/
function filtre_dsfr_tableau_attributs_supprimer_dist(array $tableau_attributs, ?string $nom): array {

	if ( $nom !== null && $nom !== '' && is_string($nom) ) {
		$nom = trim($nom);

		if ( !empty($nom) && array_key_exists($nom,$tableau_attributs) ) {
			unset($tableau_attributs[$nom]);
		}
	}

	return (array) $tableau_attributs;
}