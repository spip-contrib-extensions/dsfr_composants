<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_attribut_class`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Transforme un tableau ou une chaine de texte en `nom_de_classe_css` pouvant être utilisé
 * en toute sécurité pour l'attribut `class` d'une balise HTML.
 *
 * @example
 *     `<balise[ class="(#TEXTE|dsfr_attribut_class)"]></balise>`
 *     `<balise[ class="(#LISTE{nom_de_classe_css,autre_nom_de_classe_css}|dsfr_attribut_class)"]></balise>`
 *     `<balise[ class="(#VAL{nom_de_classe_css autre_nom_de_classe_css}|dsfr_attribut_class)"]></balise>`
 * 
 * @param string|array $valeur
 * 
 * @return string
 **/
function filtre_dsfr_attribut_class_dist($valeur): string {
	if ( is_string($valeur) ) {
		$valeur = explode(' ',trim($valeur));
	}

	if ( is_array($valeur) ) {
		$valeur = array_map('trim', $valeur);
		$valeur = array_filter($valeur);
		$valeur = array_unique($valeur);
	}

	if ( !is_array($valeur) || empty($valeur) ) {
		return '';
	}

	return implode(' ', array_map('attribut_html', $valeur));
}