<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_trim`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Fait la même chose que la fonction `trim()` de PHP mais sans couiner si
 * la valeur est `NULL` !
 * 
 * @param ?string $texte
 *     Texte à traiter
 * 
 * @return string
 *     Texte transformé (ou pas ^^)
 **/
function filtre_dsfr_trim_dist(?string $texte): string {
	if ( $texte === null || $texte === '' ) {
		return '';
	}

	// supprime les espaces et sauts de ligne `\n\r\t\v\x00` au début et à la fin
	$texte = trim($texte);

	return $texte;
}