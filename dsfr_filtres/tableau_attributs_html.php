<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_tableau_attributs_html`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Transforme un tableau en attributs HTML pouvant être utilisés
 * en toute sécurité avec une balise HTML.
 *
 * @example
 *     `<balise[ (#ARRAY|dsfr_tableau_attributs_html)]></balise>`
 * 
 * @param array $tableau_attributs
 * 
 * @return string
 **/
function filtre_dsfr_tableau_attributs_html_dist(array $tableau_attributs): string {
	if ( empty($tableau_attributs) ) {
		return '';
	}

	$attributs_html = [];
	foreach ( $tableau_attributs as $nom => $valeur ) {
		$valeur = trim($valeur);
		if ( $valeur !== '' ) {
			if ( $valeur == 'booleen' )	{
				$attributs_html[] = $nom;
			}
			// ne pas enlever les balises html pour l'attribut `placeholder`
			else if ( $nom == 'placeholder' ) {
				$attributs_html[] = $nom.'="'.attribut_html($valeur, false).'"';
			}
			else {
				$attributs_html[] = $nom.'="'.attribut_html($valeur).'"';
			}
		}
	}

	return !empty($attributs_html) ? implode(' ', $attributs_html) : '';
}