<?php

if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Filtre `|dsfr_env_nettoyer`
 * 
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 * 
 * Filtre qui nettoye le contexte de l'environnement du squelette en préservant les paramètres indiqués
 *
 * @example
 *     `[(#VAL{nom_parametre_a_preserver}|dsfr_env_nettoyer)]`
 * 
 *     `[(#VAL{nom_parametre_a_preserver autre_nom_parametre_a_preserver}|dsfr_env_nettoyer)]`
 *     `[(#LISTE{nom_parametre_a_preserver,autre_nom_parametre_a_preserver}|dsfr_env_nettoyer)]`
 * 
 * @param array $Pile
 * @param string|array $preserver
 * @param bool $preserver_spip_env
 * 
 * @return string ''
 **/
function filtre_dsfr_env_nettoyer_dist(&$Pile, $preserver, $preserver_spip_env = true) {
	if ( !isset($Pile[0]) || !is_array($Pile[0]) ) {
		return '';
	}

	// sécurité pour interdire de "tout voir" lors d'une pagination (`debut_?=-1`)
	foreach ( $Pile[0] AS $cle => $valeur ) {
		if ( is_string($cle) && str_starts_with($cle, 'debut_') ) {
			if ( !is_numeric($valeur) || intval($valeur) < 0 ) {
				$Pile[0][$cle] = '0';
			}
		}
	}

	$parametres_a_preserver = [];

	if ( is_string($preserver) ) {
		$parametres_a_preserver = explode(' ',trim($preserver));
	}
	else if ( is_int($preserver) ) {
		$parametres_a_preserver[] = $preserver;
	}
	else if ( is_array($preserver) ) {
		$parametres_a_preserver = $preserver;
	}

	if ( $preserver_spip_env ) {
		$parametres_a_preserver[] = 'spip_env';
	}

	// `$Pile` n'est pas modifié en utilisant `filtrer()` :/
	$dsfr_env_definir = chercher_filtre('dsfr_env_definir');

	return $dsfr_env_definir($Pile, NULL, $parametres_a_preserver);
}