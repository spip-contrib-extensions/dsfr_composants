# Dossier `dsfr_placeholders/`

Dossier des placeholders DSFR.


## Surcharger un placeholder

Ce dossier peut être `surchargé`. Créez le dossier `dsfr_placeholders/` dans le dossier
de votre propre plugin ou dans votre dossier `squelettes/` de SPIP. Vous pourrez ensuite
ajouter vos propres placeholders dans ce dossier qui seront prioritaires sur les placeholders
originaux du plugin.


## Ajouter un nouveau placeholder

Vous pouvez compléter les placeholders disponibles en ajoutant vos propres placeholders au 
format `reference_de_votre_placeholder.extention` dans le dossier `dsfr_placeholders/`.
Vous pourrez alors utiliser ce nouveau placeholder dans vos squelettes avec la balise
`#DSFR_PLACEHOLDER{reference_de_votre_placeholder}`. La référence du placeholder pourra aussi être utilisée
en paramètre de certains composants DSFR (par exemple avec le composant `tuile`).

### Extensions

De préférence, veillez à utiliser le format `svg` pour vos placeholders. Cependant plusieurs
formats sont supportés par ordre de priorité : `svg`,`webp`,`png`,`gif`,`jpg`. Consultez la documentation
pour savoir comment utiliser la balise `#DSFR_PLACEHOLDER` et obtenir le format désiré.


## Documentation

@see documentation/balises/dsfr_placeholder