# Changelog : DSFR Composants

Changelog du plugin SPIP **DSFR Composants**. Pour connaitre les changements concernant le DSFR vous
pouvez consulter son changelog ici : https://github.com/GouvernementFR/dsfr/blob/main/CHANGELOG.md


## 2.4.5 - 2024-11-13

### Added

- Filtre SPIP `|dsfr_tableau_ajouter`
- Filtre SPIP `|dsfr_tableau_ajouter_apres`
- Icône `arrow-up-down-line.svg`


## 2.4.4 - 2024-10-04

### Fixed

- CSS du plugin `banniere`
- CSS des `iframe` par défaut et dans l'espace privé
- CSS pour ne pas afficher le soulignement DSFR des liens sur une image

### Added

- Filtre SPIP `|dsfr_env_definir`
- Filtre SPIP `|dsfr_env_supprimer`

### Changed

- Renommage du filtre SPIP `|dsfr_textebrut` en `|dsfr_texte_brut`
- Paramètres du squelette `dsfr_composants/barre_de_recherche.html`
- Nettoyage et optimisation du code


## 2.4.3 - 2024-07-19

### Changed

- Incompatibilité avec le DSFR v1.12.* pour le moment ! Après d'autre test, le `Changements destructifs` des tableaux rend le fonctionnement des tableau aléatoire. Parfois les tableaux s'affichent, et parfois non (sans message d'erreur...) !
- Re-mise à jour de la borne de compatibilité avec la librairie DSFR en `[1.11.0;1.11.*]`


## 2.4.2 - 2024-07-19

### Fixed

- Plugin toujours en `dev` et non stable (fix du changement malvenu de @JamesRezo)


## 2.4.1 - 2024-07-19

### Changed

- Après moultes tests le plugin est compatible DSFR v1.12.1, la borne de compatibilité avec la librairie DSFR est maintenant `[1.11.0;1.12.*]`


## 2.4.0 - 2024-07-19

### Changed

- Surcharge simplifiée d'un squelette en fonction de l'espace public/privé
- Utiliser `filtrer()` au lieu d'une fonction spécifique !
- Borne du compatibilité avec la librairie DSFR suite au `Changements destructifs` des tableaux


## 2.3.0 - 2024-07-05

### Fixed

- Ne pas mettre de balise dans un commentaire !
- `safehtml` trop restrictif qui mange les commentaires
- Prise en charge des `#NOTES` par le raccourci `<exemple_de_code>` du plugin `coloration_syntaxique`
- Style du bouton copier du plugin `coloration_syntaxique`
- Sécurité `exemple_de_code`
- Composant DSFR `sommaire` qui a beaucoup trop de marge/padding si il y a une grande profondeur
- Pas de taille `chapo` pour le composant `citation`
- Citations avec des guillemets au début et à la fin
- Icône des emails à droite

### Added

- Compatibilité SPIP 4.*
- Le traitement de mise en conformité DSFR des liens ajoute le texte `ouvre une nouvelle fenêtre` au titre des liens externes
- Des logos SPIP au design DSFR !

### Changed

- Éviter les confusions `attribut_id`, `attribut_class` et `bandeau_d_information_importante`
- Nouvelle gestion des filtres DSFR
- Pipelines dans un dossier spécifique
- Optimisations et intégration du plugin `coloration_syntaxique`

## 2.2.0 - 2024-06-06

### Fixed

- CSS des notes
- Fonctionnement des ancres dans un conteneur invisible

### Added

- Balise SPIP `#DSFR_UNIQID`
- Composant `sommaire`
- Filtre SPIP `|dsfr_ancre`
- Traitements automatiques des ancres des intertitres


## 2.1.0 - 2024-05-27

### Added

- Composant `accordeon`
- Composant `groupe_d_accordeons`


## 2.0.4 - 2024-05-24

### Fixed

- Valeur par défaut des paramètres `taille` et `type` du composant `alerte`
- Forcer la `taille` du composant `alerte` en fonction du `titre`/`texte`

### Added

- Squelette du composant DSFR `tableau`

### Changed

- Optimisation `include_spip`
- Utilisation du composant `tableau` avec la fonction `dsfr_composants_traiter_balises_table()`
- Paramètres du composant `tableau`
- Suppression du paramètre `taille` du composant `mise_en_avant`


## 2.0.3 - 2024-05-17

### Fixed

- Mise en conformité DSFR des liens par défaut (les notes sont stylisées en CSS maintenant)
- Démonstration de la pagination fonctionnelle même avec un petit nombre d'articles


## 2.0.2 - 2024-05-16

### Changed

- Renommage du nom du paramètre `bordure` du raccourci typographique `dsfr-tableau` en `bordures_entre_chaque_ligne`


## 2.0.1 - 2024-05-14

### Added

- Mise en conformité DSFR des puces SPIP sur l'espace public


## 2.0.0 - 2024-05-13

### Added

- **demo** : bouton pour basculer le paramètre d'affichage DSFR (light/dark)
- Fonction `dsfr_uniqid()`
- Prise en charge des erreur `503` qui doivent avoir un squelette spécifique `standelone`
- Mise en conformité DSFR de l'affichage des `Notes et références SPIP`
- Mise en conformité DSFR de l'affichage des `Tableaux SPIP`
- Mise en conformité DSFR de l'affichage des `Liens SPIP`
- Configuration du DSFR avec des constantes (voir `dsfr_composants_options.php`)
- Documentation de la configuration du DSFR
- Si le plugin `mathjax` est utilisé, surcharge sa configuration pour désactiver le menu contextuel de `mathjax` qui n'est pas conforme au DSFR
- Modèles de pagination `modeles/pagination.html` et `modeles/pagination_precedent_suivant.html`

### Changed

- Utilisation du plugin `DSFR Lib` pour l'inclusion de la librairie DSFR
- Changement de la numérotation des versions du plugin (qui ne sera plus en corrélation avec la version de la lib DSFR)
- Meilleur gestion de l'affichage des blocs de code
- Optimisation et nettoyage du code

### Fixed

- Balise SPIP `#TRI` DSFR désactivée sur l'espace privé
- Alerte `deprecated` de la fonction `trim()` dans `filtre_dsfr_attribut_definir_dist()`
- **demo** : copier/coller du code d'inclusion SPIP dans le presse-papiers

### Removed

- Fonction `dsfr_composants_configuration_par_defaut()`
- Fonction `dsfr_composants_configuration_existante()`
- Fonction `dsfr_composants_configuration()`
- Fonction `dsfr_composants_purger()`
- Pipeline SPIP `configurer_dsfr_composants_par_defaut`
- Pipeline SPIP `configurer_dsfr_composants`
- Formulaire SPIP `configurer_dsfr_composants`
- `dsfr_composants_administrations.php`


## 1.11.2.1 - 2024-03-12

### Added

- Paramètre `attributs` pour les composants DSFR `pictogramme` et `icone`


## 1.11.2.0 - 2024-03-08

### Changed

- Mise à jour du DSFR à la version 1.11.2
- Déplacements et renommages des fichiers `dsfr_composants_pipelines.php`,
`dsfr_composants_pipelines_autoriser.php` et `style_prive_plugin_dsfr_composants.html` pour
mieux correspondre à l'architecture générale des plugins SPIP.
- Optimisation et nettoyage du code


## 1.11.1.2 - 2024-02-23

### Added

- `dsfr_composants/navigation_principale.html`

### Changed

- Déplacement des fichiers `css` et `javascript` dans leur dossier respectif
- Optimisation des pipelines `affichage_final`
- Optimisation et nettoyage du code
- Amélioration de la documentation
- Déplacement de la page de démonstration dans le dossier `demo`


## 1.11.1.1 - 2024-02-15

### Added

- `dsfr_composants/gestionnaire_de_consentement.html`
- `dsfr_composants/gestionnaire_de_consentement.js.html`
- `dsfr_composants/gestionnaire_de_consentement_bandeau.html`
- Filtre SPIP `|filtre_dsfr_texte_paragraphe`
- Conformité de la balise SPIP `#TRI` avec le DSFR


## 1.11.1.0 - 2024-02-06

### Added

- Sécurisation des liens externes afin d'éviter toute possibilité d'exécution de code malveillant
avec `rel="noopener external"` pour les composants `carte,tuile,lien,tag et bouton`
- `dsfr_composants/parametre_d_affichage.html`

### Changed

- Mise à jour du DSFR à la version 1.11.1
- Les fonctions du plugin `inc/dsfr_composants` sont chargées systématiquement

### Fixed

- Espacement des boutons admin


## 1.11.0.1 - 2024-01-31

### Added

- Balise meta `name=format-detection`
- Paramètre `taille` du composant `mise_en_exergue`
- Paramètre `liens_texte_longueur_max` du composant `fil_d_ariane`
- Les liens externes des composants `carte,tuile,lien,tag et bouton`, ouvrent
une nouvelle fenêtre par défaut (`target=_blank`)
- Pipeline SPIP `configurer_dsfr_composants_par_defaut`
- Log des modifications des valeurs de configuration du plugin
- Boutons admin au format DSFR
- Previsualisation au format DSFR
- Bouton admin DSFR avec le pipeline `formulaire_admin`

### Changed

- Ajout de l'option `chapo` pour le paramètre `taille` du composant `mise_en_avant`
- Ajout de l'option `chapo` pour le paramètre `taille` du composant `citation`
- Les formulaires de configuration du plugin sont maintenant dans le dossier `formulaires`


## 1.11.0.0 - 2023-12-19

### Changed

- Mise à jour du DSFR à la version 1.11.0
- La balise SPIP `#DSFR_COULEURS` retourne maintenant les valeurs
- La balise SPIP `#DSFR_ICONES` retourne maintenant les valeurs
- La balise SPIP `#DSFR_PICTOGRAMMES` retourne maintenant les valeurs


## 1.10.2.1 - 2023-11-24

### Added

- Désactivation de la configuration de la "Boîte Multimédia" qui est désactivée sur le site public
- Pipeline SPIP `configurer_dsfr_composants`
- Paramètre `class` pour tous les composants DSFR
- Documentation associée

### Changed

- Configuration du plugin dans l'espace privé de SPIP sur 2 colonnes
- Renommage de la fonction `dsfr_composants_lister` en `dsfr_composants_lister_tableau` pour
une meilleure clarté du code
- Optimisation du code de la fonction `dsfr_composants_config` en utilisant la fonction SPIP `table_valeur`
- Nouveau paramètre de la fonction `dsfr_composants_configuration_par_defaut` permettant de
préciser une clé de configuration
- Formulaire de configuration unique avec un `id` en paramètre #FORMULAIRE_CONFIGURER_DSFR_COMPOSANTS
- Optimisation du vidage du cache de SPIP
- Meilleur gestion de la mise à jour du plugin
- Optimisation de la documentation


## 1.10.2.0 - 2023-11-06

### Added

- Documentation des dossiers spécifiques (dsfr_composants, dsfr_icones, dsfr_pictogrammes, dsfr_placeholders)
- Pipeline SPIP `dsfr_composants_ieconfig_metas`

### Changed

- Mise à jour du DSFR à la version 1.10.2


## 1.10.1.5 - 2023-10-27

### Added

- Autorisations du plugin (`configurer` et `documentation`)
- Documentation des pipelines
- Documentation des filtres
- Balise SPIP `#DSFR_ICONE`
- Balise SPIP `#DSFR_ICONES`
- Balise SPIP `#DSFR_COULEURS`
- Balise SPIP `#DSFR_PICTOGRAMMES`
- Balise SPIP `#DSFR_PLACEHOLDER`
- Pipeline SPIP `configurer_dsfr_liste_icones`
- Pipeline SPIP `configurer_dsfr_liste_couleurs`
- Pipeline SPIP `configurer_dsfr_liste_pictogrammes`
- Filtre SPIP `|dsfr_attribut_definir`
- Filtre SPIP `|dsfr_attribut_supprimer`
- Filtre SPIP `|dsfr_attributs`
- Filtre SPIP `|dsfr_class`
- Filtre SPIP `|dsfr_class_ajouter`
- Filtre SPIP `|dsfr_class_supprimer`
- `dsfr_composants/carte.html`
- `dsfr_composants/carte.demo.html`
- `dsfr_composants/icone.html`
- `dsfr_composants/icone.demo.html`
- `dsfr_composants/groupe_de_tags.html`
- `dsfr_composants/groupe_de_tags.demo.html`
- `dsfr_composants/tag.html`
- `dsfr_composants/tag.demo.html`
- `dsfr_placeholders/1x1.png`
- `dsfr_placeholders/1x1.svg`
- `dsfr_placeholders/9x16.png`
- `dsfr_placeholders/9x16.svg`
- `dsfr_placeholders/16x9.png`
- `dsfr_placeholders/16x9.svg`

### Changed

- Utilisation de `Vanilla JS` au lieu de `jQuery` pour améliorer la pérennité du code dans l'espace privé
- Documentation du plugin dans l'espace privé de SPIP sur 2 colonnes


## 1.10.1.4 - 2023-10-11

### Added

- Documentation des balises
- Documentation des composants
- Image de chargement des balises `iframe` de la documentation
- `dsfr_composants/tuile.html`
- `dsfr_composants/tuile.demo.html`


## 1.10.1.3 - 2023-10-10

### Added

- Documentation du plugin dans l'espace privé de SPIP
- Balise SPIP `#DSFR_PICTOGRAMME`
- `dsfr_composants/citation.html`
- `dsfr_composants/citation.demo.html`
- `dsfr_composants/pictogramme.html`
- `dsfr_composants/pictogramme.demo.html`
- Utilisation de `prismjs`, une librairie javascript de coloration syntaxique pour la configuration
du plugin grâce au pipeline SPIP `affichage_final_prive`

### Changed

- Meilleure gestion de `DSFR_COOKIE()` en prenant en compte le `path`
- Meilleure gestion du pipeline SPIP `affichage_final`


## 1.10.1.2 - 2023-10-02

### Added

- Configuration du plugin `exec=configurer_dsfr_composants`
- Filtre SPIP `|dsfr_id`
- `dsfr_composants/badge.html`
- `dsfr_composants/badge.demo.html`
- `dsfr_composants/barre_de_recherche.html`
- `dsfr_composants/barre_de_recherche.demo.html`
- `dsfr_composants/fil_d_ariane.html`
- `dsfr_composants/fil_d_ariane.demo.html`
- `dsfr_composants/groupe_de_badges.html`
- `dsfr_composants/groupe_de_badges.demo.html`
- `dsfr_composants/liens_d_evitement.html`
- `dsfr_composants/liens_d_evitement.demo.html`

### Changed

- `dsfr_composants/alerte.html`, ajout du paramètre `icone`
- `dsfr_composants/alerte.html`, le paramètre `type` est maintenant vide par défaut


## 1.10.1.1 - 2023-09-27

### Added

- Fonction javascript `DSFR_COOKIE()`
- `dsfr_composants/alerte.html`
- `dsfr_composants/alerte.js.html`
- `dsfr_composants/alerte.demo.html`
- `dsfr_composants/bandeau_information_importante.html`
- `dsfr_composants/bandeau_information_importante.js.html`
- `dsfr_composants/bandeau_information_importante.demo.html`
- `dsfr_composants/bouton.html`
- `dsfr_composants/bouton.demo.html`
- `dsfr_composants/groupe_de_boutons.html`
- `dsfr_composants/groupe_de_boutons.demo.html`
- `dsfr_composants/groupe_de_liens.html`
- `dsfr_composants/groupe_de_liens.demo.html`
- `dsfr_composants/lien.html`
- `dsfr_composants/lien.demo.html`
- `dsfr_composants/mise_en_avant.html`
- `dsfr_composants/mise_en_avant.demo.html`
- `dsfr_composants/mise_en_exergue.html`
- `dsfr_composants/mise_en_exergue.demo.html`


## 1.10.1.0 - 2023-09-19

### Changed

- Mise à jour du DSFR à la version 1.10.1


## 1.10.0.0 - 2023-08-22

### Added

- Balise SPIP `#DSFR`
- Balise SPIP `#DSFR_VERSION`

### Changed

- Mise à jour du DSFR à la version 1.10.0


## 1.9.3.0 - 2023-05-22

### Changed

- Mise à jour du DSFR à la version 1.9.3


## 1.9.2.0 - 2023-04-24

### Changed

- Mise à jour du DSFR à la version 1.9.2


## 1.9.1.0 - 2023-04-17

### Changed

- Mise à jour du DSFR à la version 1.9.1


## 1.9.0.0 - 2023-03-27

- Version initiale du plugin SPIP avec la version 1.9.0 du DSFR