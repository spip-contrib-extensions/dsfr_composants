# Dossier `dsfr_composants/`

Dossier des composants DSFR du plugin.


## Inclure un composant

Vous pouvez inclure les composants DSFR avec la syntaxe d'inclusion de SPIP `<INCLURE>` ou `#INCLURE` (en statique).

Exemple :

	<INCLURE{fond=dsfr_composants/nom_du_composant}>
	#INCLURE{fond=dsfr_composants/nom_du_composant}


## Surcharger un composant

Ce dossier peut être `surchargé`. Créez le dossier `dsfr_composants/` dans le dossier
de votre propre plugin ou dans votre dossier `squelettes/` de SPIP. Vous pourrez ensuite
ajouter vos propres squelettes de composants dans ce dossier qui seront prioritaires sur les squelettes de composants
originaux du plugin.


## Ajouter un nouveau composant

Pour ajouter un nouveau composant, créez un fichier nommé `nom_du_composant.html` dans votre
dossier de surcharge `dsfr_composants/`.

Ce composant sera automatiquement ajouté à la documentation du plugin. Pour documenter votre
composant, veuillez ajouter une remarque (avec la balise SPIP `#REM`) dans l'en-tête de votre
fichier nommé `nom_du_composant.html`

Exemple :

	[(#REM)

		Nom de votre composant

		@author		Prénom NOM
		@license	GPL - https://www.gnu.org/licenses/gpl-3.0.html

		@param nom_du_parametre
			Description du paramètre `nom_du_parametre`

		@param nom_autre_parametre
			Description du paramètre `nom_autre_parametre`
	]


## Démonstration

Une démonstration automatiquement chargée par la documentation peut être associée à votre composant.
Pour proposer une démonstration, créer un fichier nommé `nom_du_composant.demo.html` dans votre
dossier de surcharge `dsfr_composants/`.


## Documentation

@see documentation/composants