<?php
/**
 * Fonctions d'autorisations du plugin SPIP
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Fonction d'appel pour le pipeline
 * 
 * @pipeline autoriser
 **/
function dsfr_composants_autoriser() { }

/**
 * Autorisation d'accéder à la documentation du plugin.
 * Il faut être webmestre.
 *
 * @param string         $faire   Action demandée : `documentation`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_dsfr_composants` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $opt     Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool          `true` si l'auteur est autorisée à exécuter l'action, `false` sinon.
**/
function autoriser_dsfrcomposants_documentation_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('webmestre', null, 0, $qui, $opt);
}

/**
 * Autorisation de voir l'élément de menu menant à la page de documentation du plugin.
 * Il faut être autorisé à voir la documentation du plugin.
 *
 * @param string         $faire   Action demandée : `menu`
 * @param string         $type    Type d'objet sur lequel appliquer l'action : `_dsfr_composants` (ce n'est pas un objet au sens SPIP)
 * @param int            $id      Identifiant de l'objet : `0`, inutilisé
 * @param null|array|int $qui     L'initiateur de l'action:
 *                                - si null on prend alors visiteur_session
 *                                - un id_auteur (on regarde dans la base)
 *                                - un tableau auteur complet, y compris [restreint]
 * @param null|array     $opt     Tableau d'options sous forme de tableau associatif : `null`, inutilisé
 *
 * @return bool          `true` si l'auteur est autorisée à exécuter l'action, `false` sinon.
 **/
function autoriser_documentationdsfrcomposants_menu_dist($faire, $type, $id, $qui, $opt) {
	return autoriser('documentation', '_dsfr_composants', $id, $qui, $opt);
}

/**
 * Sur le site public, la "Boîte Multimédia" est désactivé avec le pipeline `dsfr_composants_mediabox_config()`.
 * On désactive donc la configuration de la "Boîte Multimédia" dans l'espace privé car ça n'a plus d'intérêt.
 */
function autoriser_mediabox_configurer($faire, $type, $id, $qui, $opt) { return false; }
function autoriser_mediabox_menu($faire, $type, $id, $qui, $opt) { return false; }