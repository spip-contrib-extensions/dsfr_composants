<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline compagnon_messages
 * 
 * @param array $flux
 * 
 * @return array
 **/
function dsfr_composants_compagnon_messages($flux) {

	// uniquement sur l'espace privé
	if ( test_espace_prive() ) {

		$exec = $flux['args']['exec'];
		$pipeline = $flux['args']['pipeline'];
		$aides = &$flux['data'];

		switch ( $pipeline ) {
			case 'affiche_milieu':
				switch ( $exec ) {
					case 'documentation_dsfr_composants':
						$aides[] = [
							'id' => 'dsfr_composants',
							'titre' => <<<HEREDOC_TITRE
								Qu'est-ce que le Système de Design de l'État ?
								HEREDOC_TITRE,
							'texte' => <<<HEREDOC_TEXTE
								<p>
									Ce plugin fait partit du volet numérique de la marque de l'État, qui permet pour les citoyens d'avoir une cohérence 
									graphique et une meilleure expérience à travers l'ensemble des sites de l'État.
								</p>
								<p>
									Le Système de Design de l'État regroupe un ensemble de composants réutilisables, 
									répondant à des standards et à une gouvernance, pouvant être assemblés pour créer
									des sites Internet accessibles et ergonomiques.
								</p>
								<p>
									<a href="https://www.systeme-de-design.gouv.fr" target="_blank" class="btn">Prendre en main le Système de Design</a>
								</p>
							HEREDOC_TEXTE,
							'statuts' => ['webmestre']
						];
						break;
				}
				break;
		}

	}

	return $flux;
}