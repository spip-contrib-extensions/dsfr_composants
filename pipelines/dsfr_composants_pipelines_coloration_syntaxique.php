<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline coloration_syntaxique_css
 * 
 * @param array $flux
 * 
 * @return array
 **/
function dsfr_composants_coloration_syntaxique_css($flux) {

	// styles CSS spécifiques au DSFR uniquement sur l'espace public
	if ( !test_espace_prive() ) {
		$flux['css/coloration_syntaxique_dsfr_composants.css'] = produire_fond_statique('css/coloration_syntaxique_dsfr_composants.css');
	}

	return $flux;
}

/**
 * @pipeline exemple_de_code_source
 * 
 * @param array $flux
 * 
 * @return $flux
 **/
function dsfr_composants_exemple_de_code_source($flux) {

	// ajoute un accordéon si il y a beaucoup de lignes de code sur l'espace public
	if ( !test_espace_prive() && intval($flux['args']['nombre_de_lignes']) > 15 ) {
	
		$flux['data'] = recuperer_fond('dsfr_composants/accordeon',[
			'titre'	=> _T('dsfr_composants:extrait_de_code'),
			'texte'	=> $flux['data'],
		]);
	}
	else if ( test_espace_prive() ) {
		// ne pas afficher le code source de l'exemple sur l'espace privé
		return '';
	}

	return $flux;
}