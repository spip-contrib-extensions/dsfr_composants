<?php
/**
 * Pipelines SPIP utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Déclarer les interfaces des tables SPIP
 * 
 * Permet de définir des traitements automatiques sur certaines
 * balises SPIP. L'étoile (#BALISE*) désactive ces traitements.
 *
 * @pipeline declarer_tables_interfaces
 *
 * @param array $interface
 * 		Déclarations d'interface pour le compilateur
 * @return array $interface
 * 		Déclarations d'interface complétée pour le compilateur
 **/
function dsfr_composants_declarer_tables_interfaces($interface) {

	// ajoute une ancre aux intertitres de toutes les balises SPIP `#TEXTE`
	defined('_DSFR_TRAITEMENT_AUTOMATIQUE_ANCRE_INTERTITRES') || define('_DSFR_TRAITEMENT_AUTOMATIQUE_ANCRE_INTERTITRES', false);
	if ( _DSFR_TRAITEMENT_AUTOMATIQUE_ANCRE_INTERTITRES ) {
		if ( !defined('_TRAITEMENT_DSFR_ANCRE_INTERTITRES') ) {
			define('_TRAITEMENT_DSFR_ANCRE_INTERTITRES', 'dsfr_traitement(\'ancre_intertitres\', %s, $Pile[0])');
		}
		foreach ( $interface['table_des_traitements']['TEXTE'] as $cle => $valeur ) {
			$interface['table_des_traitements']['TEXTE'][$cle] = str_replace('%s', $valeur, _TRAITEMENT_DSFR_ANCRE_INTERTITRES);
		}
	}

	// ne pas faire les traitements suivant si on est sur l'espace privé
	if ( test_espace_prive() ) {
		return $interface;
	}

	// mise en conformité DSFR de la balise SPIP `#TRI`
	defined('_DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_TRI') || define('_DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_TRI', true);
	if ( _DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_TRI ) {
		if ( !defined('_TRAITEMENT_DSFR_BALISE_TRI') ) {
			define('_TRAITEMENT_DSFR_BALISE_TRI', 'dsfr_traitement(\'balise_tri\', %s, $Pile[0])');
		}
		$interface['table_des_traitements']['TRI'][0] = _TRAITEMENT_DSFR_BALISE_TRI;
	}

	// mise en conformité DSFR de la balise SPIP `#NOTES`
	defined('_DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_NOTES') || define('_DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_NOTES', true);
	if ( _DSFR_TRAITEMENT_AUTOMATIQUE_BALISE_NOTES ) {
		if ( !defined('_TRAITEMENT_DSFR_BALISE_NOTES') ) {
			define('_TRAITEMENT_DSFR_BALISE_NOTES', 'dsfr_traitement(\'balise_notes\', %s, $Pile[0])');
		}
		$interface['table_des_traitements']['NOTES'][0] = _TRAITEMENT_DSFR_BALISE_NOTES;
	}
	
	return $interface;
}

/**
 * Ajoute le bouton d'administration indiquant la version du DSFR utilisé
 * 
 * @pipeline formulaire_admin
 * 
 * @param array $flux
 * 
 * @return array $flux
 **/
function dsfr_composants_formulaire_admin($flux) {

	// ne pas ajouter les boutons d'administration lors d'une erreur 503
	if ( http_response_code() == 503 ) {
		return false;
	}

	$bouton = recuperer_fond('prive/bouton/dsfr_version');
	if ( !empty($bouton) ) {
		$extra = '<!--extra-->';
		$flux['data'] = str_ireplace($extra, "\n\t".$bouton."\n\t".$extra, $flux['data']);
	}

	return $flux;
}

/**
 * @pipeline insert_head_css
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_composants_insert_head_css($flux) {

	// ajoute les feuilles de styles CSS du DSFR.
	$dsfr_css = charger_fonction('dsfr_css', 'inc');

	$ajouter_css = '';
	foreach ( $dsfr_css() AS $css ) {
		$css = trim($css);

		if ( !empty($css) ){
			$ajouter_css .= "\n\t".$css;
		}
	}

	if ( !empty($ajouter_css) ) {
		$flux .= "\n\t".'<!-- dsfr_css -->'.$ajouter_css;
	}

	return $flux;
}

/**
 * @pipeline affichage_final
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_composants_affichage_final($flux) {

	// ne rien afficher
	if ( !$flux || !isset($GLOBALS['html']) || !$GLOBALS['html'] ) {
		return $flux;
	}

	// ne rien faire lors d'une erreur 503
	if ( http_response_code() == 503 ) {
		return $flux;
	}

	$flux = trim($flux); // enlève les espaces intempestifs générés par les commentaires des squelettes SPIP !

	// supprimer la class `dsfr_lien_ok` ajoutée par le traitement `mise_en_conformite_liens`
	if ( strpos($flux, 'dsfr_lien_ok') !== false ) {
		$flux = str_replace(' dsfr_lien_ok', '', $flux);
		$flux = str_replace('dsfr_lien_ok ', '', $flux);
		$flux = str_replace('class="dsfr_lien_ok"', '', $flux); 
	}

	/*
		les traitements qui suivent ne doivent pas être effectués lors d'une requette ajax
		ils nécessitent une page complète avec les balises `<html>` et `</body>`
	*/
	if ( defined('_AJAX') && _AJAX ) {
		return $flux;
	}

	// nécessaire pour `timestamp()` et `produire_fond_statique()`
	include_spip('inc/filtres');

	// ajoute data-fr-scheme="system" à la balise `<html>`
	if ( !preg_match('/<html[^>]*data-fr-scheme/i', $flux) ) {
		$flux = preg_replace('/<html(.*)>/i','<html$1 data-fr-scheme="system">', $flux);
	}

	// mise en conformité de la balise `<head>`
	if ( preg_match('/<head[^>]*>(.*)<\/head>/Uims', $flux, $balise_head_trouvee) ) {
		$balise_head_contenu = $balise_head_trouvee[1];
		$balise_head_contenu_position = strpos($flux, $balise_head_contenu);
		$balise_head_contenu_longueur = strlen($balise_head_contenu);

		// remplacement du contenu
		$dsfr_head = charger_fonction('dsfr_head', 'inc');
		$flux = substr_replace($flux, "\n\t".$dsfr_head($balise_head_contenu)."\n", $balise_head_contenu_position, $balise_head_contenu_longueur);	
	}

	// ajoute les fichiers javascript du DSFR.
	// le commentaire `<!-- dsfr_javascript -->` très important et doit être conservé !
	// il permet d'ajouter du contenu supplementaire juste avant le chargement de l'ensemble
	// des fichiers javascript nécessaire au bon fonctionnement du DSFR (utilisé par le plugin `DSFR Squelettes`).
	$dsfr_javascript = charger_fonction('dsfr_javascript', 'inc');

	$ajouter_javascript = '';
	foreach ( $dsfr_javascript() AS $js ) {
		$js = trim($js);

		if ( !empty($js) ){
			$ajouter_javascript .= "\n".$js;
		}
	}

	// ajoute le contenu juste avant la fermeture de la balise `<body>`
	if ( !empty($ajouter_javascript) ) {
		$flux = str_replace('</body>', "\n".'<!-- dsfr_javascript -->'.$ajouter_javascript."\n\n".'</body>', $flux);
	}

	return $flux;
}

/**
 * @pipeline mediabox_config
 * 
 * @see message_page_indisponible()
 * 
 * @param array $contexte
 * 
 * @return array
 **/
function dsfr_composants_page_indisponible($contexte) {

	// gestion de l'erreur 503 qui doit utiliser un squelette spécifique
	if ( $contexte['status'] == '503' ) {
		$contexte['code'] = '503 Service Unavailable';
		$contexte['fond'] = '503';
	}

	return $contexte;
}

/**
 * @pipeline post_echappe_html_propre
 * 
 * @param string $flux
 * 
 * @return string
 **/
function dsfr_composants_post_echappe_html_propre($flux) {
	// flux vide
	if ( !$flux || empty($flux) ) {
		return $flux;
	}

	// ne pas faire les traitements suivant si on est sur l'espace privé
	if ( test_espace_prive() ) {
		return $flux;
	}

	// mise en conformité DSFR des tableaux SPIP (`<table>` qui ont la class `table`)
	$flux = dsfr_traitement('mise_en_conformite_tableaux_spip', $flux);

	// mise en conformité DSFR des liens `<a>`
	$flux = dsfr_traitement('mise_en_conformite_liens', $flux);

	return $flux;
}

/**
 * @pipeline styliser
 * 
 * @param array $flux
 * 
 * @return array
 **/
function dsfr_composants_styliser($flux) {
	$fond = $flux['args']['fond'];
	$ext = $flux['args']['ext'];

	// permet de surcharger facilement un fond en fonction de l'espace public/privé
	$espace = test_espace_prive() ? 'prive' : 'public';
	if ( $chemin = find_in_path($fond.'_'.$espace.'.'.$ext) ) {
		$flux['data'] = substr($chemin, 0, -strlen('.'.$ext));
	}

	return $flux;
}