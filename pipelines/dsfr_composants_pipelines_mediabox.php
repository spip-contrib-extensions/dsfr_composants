<?php
/**
 * Pipelines utilisés par le plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * @pipeline mediabox_config
 * 
 * @param array $config
 * 
 * @return array
 **/
function dsfr_composants_mediabox_config($config) {
	// désactive la mediabox sur le site public pour ne pas interférer avec le DSFR
	if ( !test_espace_prive() ) {
		$config['active'] = 'non';
	}

	return $config;
}