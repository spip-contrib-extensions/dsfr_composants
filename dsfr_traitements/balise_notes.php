<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Traitement de la balise SPIP `#NOTES` pour qu'elle soit en conformité avec le DSFR
 * 
 * Cette fonction est automatiquement appelée lors de l'utilisation de la balise SPIP `#NOTES` sur l'espace public
 * avec le pipeline `declarer_tables_interfaces`
 *
 * @param string $balise_notes
 *     Code HTML du résultat du calcul de la balise d'origine
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_balise_notes($balise_notes, $env = []) {
	if ( !$balise_notes ) {
		return $balise_notes;
	}
		
	// supprime cet attribut déprécié
	$balise_notes = preg_replace('/\s*rev="appendix"/', '', $balise_notes);

	// texte style DSFR SM
	$balise_notes = preg_replace('/<p[^>]*?>/', '<p class="fr-text--sm">', $balise_notes);
	// ajoute une class pour le contenu de la note
	$balise_notes = preg_replace('/<div id="nb([^"]*?)">/', '<div class="dsfr_contenu_note" id="nb'."$1".'">', $balise_notes);

	// titre du lien
	$titre_lien_reference = attribut_html(_T('dsfr_composants:revenir_plus_haut'));
	$balise_notes = preg_replace('/title="'._T('info_notes').'[^"]*?"/', 'title="'.$titre_lien_reference.'"', $balise_notes);

	return $balise_notes;
}