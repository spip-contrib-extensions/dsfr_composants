<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Traitement qui ajoute automatiquement des ancres aux intertitres présents dans le code HTML
 * 
 * Cette fonction est automatiquement appelée lors de l'utilisation de la balise SPIP `#TEXTE`
 * avec le pipeline `declarer_tables_interfaces`
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_ancre_intertitres($html, $env = []) {

	$html = traitement_dsfr_ancre_intertitres_inserer($html, $env);

	return $html;
}

/**
 * Ajoute une ancre aux intertitres présents dans le code HTML
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_ancre_intertitres_inserer($html, $env = []) {
	$class_ancre = 'dsfr_ancre_intertitre';

	// pas de code HTML ou pas d'intertitres
	if ( !$html || stripos($html, '<h') === false ) {
		return $html;
	}

	if ( !function_exists('echappe_html') ) {
		include_spip('inc/texte_mini');
	}

	// protection des blocs <html>, <code>... voir `_PROTEGE_BLOCS`
	$html = echappe_html($html, 'traitement_dsfr_ancre_intertitres_inserer', true);

	if ( preg_match_all('/<h([123456])([^>]*)>(.*)<\/h\\1>/UimsS', $html, $intertitres_trouves, PREG_SET_ORDER) ) {
		/*
			- index 0 : la capture de l'intertitre complet
			- index 1 : le niveau de l'intertire (1,2,3,4,5 ou 6)
			- index 2 : attributs HTML de l'intertire
			- index 3 : le contenu du texte compris entre les balises de l'intertitre
		*/

		include_spip('inc/filtres');

		//var_dump($intertitres_trouves);

		// tableau des ancres déjà insérées pour éviter de créer des ancres en double
		$ancres_inserees = [];

		foreach ( $intertitres_trouves as $intertitre ) {
			$intertitre_modifie = false;

			$intertitre_niveau = intval($intertitre[1]);
			$intertitre_attributs = trim($intertitre[2]);
			// trim et retransforme les blocs échappés dans le contenu du titre
			$intertitre_contenu = trim(echappe_retour($intertitre[3], 'traitement_dsfr_ancre_intertitres_inserer'));
		
			// ajoute une ancre automatiquement
			if (
				// si elle n'est pas déjà là
				strpos($intertitre_contenu, $class_ancre) === false &&
				// si l'intertitre ne fait pas parti d'un composant DSFR (ex. fr-tile__title, fr-callout__title, etc...)
				!preg_match('/fr-[a-z]*__title/i', $intertitre_attributs)
			) {

				// generer une ancre a partir du contenu
				// ajoute le texte `ancre-` au début pour éviter de créer des ancres ciblant un `id` déjà existant
				$ancre = identifiant_slug('ancre-'.$intertitre_contenu, 'ancre', [
					'separateur'	=> '-'
				]);

				// ancre déjà insérée ?
				if ( in_array($ancre, $ancres_inserees) ) {
					$i = 2;
					while ( in_array($ancre.'-'.$i, $ancres_inserees) ) {
						$i++;
					}
					$ancre = $ancre.'-'.$i;
				}
				$ancres_inserees[] = $ancre;

				$intertitre_ancre = '<a class="'.$class_ancre.'" id="'.$ancre.'" href="#'.$ancre.'"></a>';
	
				$intertitre_modifie  = '<h'.$intertitre_niveau.(!empty($intertitre_attributs) ? ' '.$intertitre_attributs : '').'>';
				$intertitre_modifie .= $intertitre_contenu.$intertitre_ancre;
				$intertitre_modifie .='</h'.$intertitre_niveau.'>';
			}

			// remplacement du contenu
			if ( $intertitre_modifie ) {
				$intertitre_position = strpos($html, $intertitre[0]);
				$intertitre_longueur = strlen($intertitre[0]);

				$html = substr_replace($html,  $intertitre_modifie, $intertitre_position, $intertitre_longueur);
			}
		}
	}

	// retransforme les blocs échappés
	$html = echappe_retour($html, 'traitement_dsfr_ancre_intertitres_inserer');

	return $html;
}

/**
 * Supprime l'ancre DSFR des intertitres présents dans le code HTML
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_ancre_intertitres_supprimer($html, $env = []) {
	$class_ancre = 'dsfr_ancre_intertitre';

	// pas de code HTML, ou pas d'intertitres, ou pas d'ancre DSFR
	if ( !$html || stripos($html, '<h') === false || strpos($html, $class_ancre) === false ) {
		return $html;
	}

	return preg_replace('/<a[^>]*class=["\'][^"\']*\b'.$class_ancre.'\b[^"\']*["\'][^>]*><\/a>/UimsS', '', $html);
}