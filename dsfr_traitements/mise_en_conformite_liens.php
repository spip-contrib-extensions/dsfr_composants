<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Mise en conformité DSFR des liens `<a>`
 * @see https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/lien/
 *
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_mise_en_conformite_liens($html, $env = []) {

	// pas de code HTML ou pas de lien
	if ( !$html || stripos($html, '<a') === false ) {
		return $html;
	}

	include_spip('inc/filtres');

	$liens_trouves = extraire_balises($html, 'a');

	if ( !empty($liens_trouves) ) {
		include_spip('inc/lien');

		foreach ( $liens_trouves as $lien ) {	
			/*
				On met les class CSS du lien dans un tableau pour faciliter les tests :
				Voir : https://www.spip.net/fr_article3377.html
	
				 - `spip_note` : lien de note
				 - `spip_glossaire` : lien vers le glossaire externe (normalement Wikipédia)
				 - `spip_mail` : lien email
				 - `spip_ancre`
				 - `spip_in` : lien interne du site (normalement relatif)
				 - `spip_out` : lien avec http(s):// (attention peut être interne comme externe)
				 - `spip_url` : URL transformées en lien hypertexte
				 - `spip_url auto` URLs brutes traitées par `tw_autoliens()`
				 - `spip_doc_lien` : lien de document généré par un modèle (plugin média)
			*/
	
			$lien_class = extraire_attribut($lien, 'class');
			$lien_class = $lien_class ? array_filter(explode(' ', $lien_class)) : [];

			// lien déjà traité
			if ( in_array('dsfr_lien_ok', $lien_class) ) {
				continue;
			}
			
			$lien_modifie = ajouter_class($lien, 'dsfr_lien_ok');
			$lien_href = extraire_attribut($lien, 'href');

			// supprime la class `spip_note` au cas ou les CSS de SPIP sont toujours actif
			if ( in_array('spip_note', $lien_class) ) {
				$lien_modifie = supprimer_class($lien_modifie, 'spip_note');
				$lien_modifie = ajouter_class($lien_modifie, 'dsfr_note');
			}

			// ajoute l'icône enveloppe
			if ( $lien_href && stripos($lien_href, 'mailto:') !== false ) {
				$lien_modifie = ajouter_class($lien_modifie, 'fr-icon-mail-line fr-link--icon-right');
			}
	
			// traitement des liens externes
			if ( $lien_href && lien_is_url_externe($lien_href) ) {
				// les liens externes ouvrent une nouvelle fenêtre par défaut
				$lien_modifie = inserer_attribut($lien_modifie, 'target', '_blank');

				// ajoute le texte : `ouvre une nouvelle fenêtre` au titre
				$texte_ouvre_une_nouvelle_fenetre = _T('dsfr_composants:ouvre_une_nouvelle_fenetre');

				$lien_titre = extraire_attribut($lien_modifie, 'title');
				$lien_titre = $lien_titre ? trim($lien_titre) : '';

				if ( $texte_ouvre_une_nouvelle_fenetre != substr($lien_titre, -strlen($texte_ouvre_une_nouvelle_fenetre)) ) {
					if ( !empty($lien_titre) ) {
						$lien_titre = $lien_titre.' - ';
					}
					else {
						$texte_ouvre_une_nouvelle_fenetre = ucfirst($texte_ouvre_une_nouvelle_fenetre);
					}
					$lien_modifie = inserer_attribut($lien_modifie, 'title', $lien_titre.$texte_ouvre_une_nouvelle_fenetre);
				}
	
				// ajoute une sécurité pour éviter l'exécution de code malveillant
				// on preserve les valeur déjà existante (ex: nofollow)
				$lien_rel = extraire_attribut($lien, 'rel');
				$lien_rel = $lien_rel ? array_filter(explode(' ', $lien_rel)) : [];
	
				$lien_rel[] = 'noopener';
				$lien_rel[] = 'external';
	
				$lien_modifie = inserer_attribut($lien_modifie, 'rel', implode(' ',array_unique($lien_rel)));
			}
	
			// le lien a été modifié (à minima par l'ajout de la class `dsfr_lien_ok`), on remplace
			$html = str_replace($lien, $lien_modifie, $html);
		}
	}

	return $html;
}