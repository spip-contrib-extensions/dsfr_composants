<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Traitement de la balise SPIP `#TRI` pour qu'elle soit en conformité avec le DSFR
 * 
 * Cette fonction est automatiquement appelée lors de l'utilisation de la balise SPIP `#TRI` sur l'espace public
 * avec le pipeline `declarer_tables_interfaces`
 * 
 * Il est possible de transmettre avec le paramètre `class` de la balise plusieurs nom de class
 * permettant de modifier à minima le lien généré :
 * 
 *  - `dsfr-lien-taille-petite`
 *  - `dsfr-lien-taille-grande`
 *
 * @param string $balise_tri
 *     Code HTML du résultat du calcul de la balise d'origine
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 *
 * @example
 *     `#TRI{date,Date}`
 *     `#TRI{date,Date,dsfr-lien-taille-petite}`
 *     `#TRI{titre,Titre,dsfr-lien-taille-grande ajax}`
 **/
function traitement_dsfr_balise_tri($balise_tri, $env = []) {
	if ( !$balise_tri ) {
		return $balise_tri;
	}

	// contexte du lien
	$lien = [
		'url'				=> extraire_attribut($balise_tri,'href'),
		'texte' 			=> textebrut($balise_tri),
		'icone'				=> 'arrow-up-down-fill',
		// utilise un tableau de class pour faciliter le traitement
		'attribut_class'	=> filtrer('dsfr_tableau_class_ajouter', [], extraire_attribut($balise_tri,'class')),
	];

	$lien['titre'] = _T('dsfr_composants:trier_par_tri',['tri' => $lien['texte']]);
	// un lien de tri est une action. Il doit toujours être hors du contenu et de type `simple`.
	$lien['type'] = 'simple';

	// traitements spéciaux en fonction du nom de certaine class
	$supprimer_class = [];
	foreach ( $lien['attribut_class'] as $class ) {
		$supprimer = false;

		// taille du lien
		if ( $class == 'dsfr-lien-taille-petite' ) {
			$lien['taille'] = 'petite';
			$supprimer_class[] = $class;
		}
		else if ( $class == 'dsfr-lien-taille-grande' ) {
			$lien['taille'] = 'grande';
			$supprimer_class[] = $class;
		}
	}

	// supprime les class "outils" 
	$lien['attribut_class'] = filtrer('dsfr_tableau_class_supprimer', $lien['attribut_class'], $supprimer_class);

	// transforme le tableau en valeur de l'attribut class
	$lien['attribut_class'] = filtrer('dsfr_attribut_class', $lien['attribut_class']);

	return recuperer_fond('dsfr_composants/lien', $lien);
}