<?php
/**
 * Fonction de traitement
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Mise en conformité DSFR des tableaux SPIP (`<table>` qui ont la class `table`)
 * @see https://www.systeme-de-design.gouv.fr/elements-d-interface/composants/tableau
 * 
 * @param string $html
 *     Code HTML
 * @param array $env
 *     Environnement
 * 
 * @return string
 *     Code HTML modifié
 **/
function traitement_dsfr_mise_en_conformite_tableaux_spip($html, $env = []) {

	// pas de code HTML ou pas de tableau
	if ( !$html || stripos($html, '<table') === false ) {
		return $html;
	}

	// les tableaux générés par textwheel qui ajoute la class `table` aux tableaux. Voir `traiter_tableau()`
	$balise = 'table'; $class = 'table';
	if ( preg_match_all('/<'.$balise.'[^>]*class=["\'][^"\']*\b'.$class.'\b[^"\']*["\'][^>]*>(?:(?!<'.$balise.')[\s\S]).*<\/'.$balise.'>/UimsS', $html, $tableaux_trouves, PREG_SET_ORDER) ) {
		//var_dump($tableaux_trouves);

		// nécessaire pour `vider_attribut()`, `supprimer_class()`, etc...
		include_spip('inc/filtres');

		foreach ( $tableaux_trouves as $tableau ) {
			$tableau_position = strpos($html, $tableau[0]);
			$tableau_longueur = strlen($tableau[0]);
			$tableau_contenu = $tableau[0];
			$tableau_env = $env;

			// supprime la class `table` pour éviter de faire plusieurs fois le même traitement
			$tableau_contenu = supprimer_class($tableau_contenu, 'table');

			// supprime la class `spip` au cas ou les CSS de SPIP sont toujours actif
			$tableau_contenu = supprimer_class($tableau_contenu, 'spip');

			// si le tableau contient `<caption>` avec uniquement un résumé SPIP (pas de légende)
			$balise = 'caption'; $class = 'summary';
			if ( preg_match('/<'.$balise.'[^>]*class=["\'][^"\']*\b'.$class.'\b[^"\']*["\'][^>]*>(?:(?!<'.$balise.')[\s\S]).*<\/'.$balise.'>/UimsS', $tableau_contenu, $caption_trouve) ) {
				// force `afficher_titre` à `non` pour ne pas afficher le `<caption>`
				$tableau_env['afficher_titre'] = 'non';
			}
			// si le tableau contient `<caption>`, mise en conformité DSFR
			if ( preg_match('/<'.$balise.'(?:(?!<'.$balise.')[\s\S]).*<\/'.$balise.'>/UimsS', $tableau_contenu, $caption_trouve) ) {
				$caption_position = strpos($tableau_contenu, $caption_trouve[0]);
				$caption_longueur = strlen($caption_trouve[0]);
				$caption_contenu = $caption_trouve[0];

				// enlève les classes SPIP inutiles
				$caption_contenu = str_replace(' class="summary offscreen"', '', $caption_contenu);
				// enlève le `<br />` du résumé inutile
				$caption_contenu = str_replace('<br />', '', $caption_contenu);
				// remplacement du contenu
				$tableau_contenu = substr_replace($tableau_contenu, $caption_contenu, $caption_position, $caption_longueur);
			}

			// si le tableau contient `<thead>`, mise en conformité des `<th>`
			$balise = 'thead';
			if ( preg_match('/<'.$balise.'(?:(?!<'.$balise.')[\s\S]).*<\/'.$balise.'>/UimsS', $tableau_contenu, $thead_trouve) ) {
				$thead_position = strpos($tableau_contenu, $thead_trouve[0]);
				$thead_longueur = strlen($thead_trouve[0]);
				$thead_contenu = $thead_trouve[0];

				// ajout du `scope`
				$thead_contenu = str_replace('<th ', '<th scope="col" ', $thead_contenu);
				// remplacement du contenu
				$tableau_contenu = substr_replace($tableau_contenu, $thead_contenu, $thead_position, $thead_longueur);
			}

			// remplacement du contenu par le composant DSFR
			$tableau_env['tableau'] = $tableau_contenu;
			$html = substr_replace($html, recuperer_fond('dsfr_composants/tableau',$tableau_env), $tableau_position, $tableau_longueur);
		}
	}

	return $html;
}