<?php
/**
 * Fonctions de modèle DSFR
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Informations et configurations du modèle
 * 
 * @return array
 **/
function dsfr_modele_composant() {
	return [
		'nom'			=> 'Composant',
	];
}

/**
 * Traitement du modèle
 * 
 * @param array $env
 * 
 * @return array
 **/
function dsfr_modele_composant_traiter($env = []) {
	$retour = [];

	// paramètres du raccourci ?
	$parametres_raccourci = array_key_exists('parametres_raccourci', $env) ? $env['parametres_raccourci'] : [];

	$composant = array_key_exists('composant', $parametres_raccourci) ? $parametres_raccourci['composant'] : false;

	// gestion des potentielles erreurs
	if ( !$composant || !trouver_fond($composant,'dsfr_composants') ) {
		return ['erreur' => 'Le composant est invalide'];
	}

	// données
	$retour['data'] = trim(recuperer_fond('dsfr_composants/'.$composant, $env));

	// retour du traitement
	return $retour;
}