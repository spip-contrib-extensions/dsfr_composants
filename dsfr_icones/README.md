# Dossier `dsfr_icones/`

Dossier des icônes DSFR.


## Surcharger une icône

Ce dossier peut être `surchargé`. Créez le dossier `dsfr_icones/` dans le dossier
de votre propre plugin ou dans votre dossier `squelettes/` de SPIP. Vous pourrez ensuite
ajouter vos propres icônes dans ce dossier qui seront prioritaires sur les icônes de la
librairie DSFR.
Veillez à conserver la même arborescence du dossier `icons/` de la librairie DSFR pour pouvoir
surcharger une icône.


## Ajouter une nouvelle icône

Vous pouvez compléter la liste des icônes disponibles en ajoutant vos propres icônes au 
format `reference_de_votre_icone.svg` dans le dossier `dsfr_icones/`.
La librairie [Remix Icons](https://remixicon.com) propose des icônes libres de droits 
que vous pouvez utiliser en plus des icônes DSFR.
Vous pourrez alors utiliser cette nouvelle icône dans vos squelettes avec la balise
`#DSFR_ICONE{reference_de_votre_icone}`. La référence de l'icône pourra aussi être utilisée
en paramètre de certains composants DSFR.

### Référence

**Attention**, le nom du fichier de votre icône (considéré comme une référence) doit être
unique même si vous la placez dans un sous-dossier de votre dossier `dsfr_icones/`.

### Catégories

Si besoin vous pouvez catégoriser vos icônes en les plaçants dans différents sous-dossiers.
Une icône placée directement à la racine du dossier `dsfr_icones/` sera considérée comme
sans catégorie. Vous pouvez ainsi facilement lister les icônes d'une catégorie avec la
balise `#DSFR_ICONES{categorie=nom_de_votre_sous_dossier}`.
Quel que soit sa catégorie, une icône garde la même référence unique.

### Cache SPIP

Le plugin génère automatiquement un fichier `css/dsfr_icones.css` contenant la liste de vos icônes.
Veuillez vider le cache de SPIP pour appliquer les changements du dossier `dsfr_icones/`.


## Documentation

@see documentation/balises/dsfr_icone
@see documentation/composants/icone