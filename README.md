# Plugin SPIP : DSFR Composants

Le plugin charge automatiquement sur l'espace public les scripts CSS et Javascript permettant d'utiliser
l'ensemble des composants DSFR. Vous devrez ensuite adapter le contenu de vos squelettes pour pouvoir 
utiliser ces différents composants.

Ce plugin est maintenu et géré par les équipes de l'[Académie d'Orléans-Tours](https://spip.ac-orleans-tours.fr).


## 🇫🇷 Système de Design de l'État

[DSFR](https://www.systeme-de-design.gouv.fr) Le Système de Design de l'État (ci-après, le **DSFR**) est un ensemble
de composants web HTML, CSS et Javascript pour faciliter le travail des équipes projets des sites Internet publics,
et créer des interfaces numériques de qualité et accessibles.

Son utilisation par les administrations est soumise à une demande d'agrément.

[Voir la documentation officielle](https://www.systeme-de-design.gouv.fr).

### Licence et droit d'utilisation

##### ⚠️ Utilisation interdite en dehors des sites Internet de l'État

>Il est formellement interdit à tout autre acteur dutiliser le Système de Design de l'État (les administrations
territoriales ou tout autre acteur privé) pour des sites web ou des applications. Le Système de Design de l'État
représente l'identité numérique de l'État. En cas d'usage à des fins trompeuses ou frauduleuses, l'État se réserve
le droit d'entreprendre les actions nécessaires pour y mettre un terme.

Voir les [conditions générales d'utilisation](https://github.com/GouvernementFR/dsfr/blob/main/doc/legal/cgu.md).

##### ⚠️ Prohibited Use Outside Government Websites

>This Design System is only meant to be used by official French public services' websites and apps. Its main purpose
is to make it easy to identify governmental websites for citizens. See terms.


## Compatibilité et prérequis du plugin SPIP

Le plugin est compatible avec :

 * [SPIP 4.2+](https://www.spip.net)
 * [PHP 8.1+](https://www.php.net)

Il nécessite préalablement l'installation du plugin SPIP [DSFR Lib](https://contrib.spip.net/DSFR-Lib)


## Documentation

Toute la documentation du plugin est disponible depuis l'espace privé de SPIP dans le menu `Développement`.
Vous devez `Configurer vos préférences de menus` pour faire apparaître ce menu.


## Fonctionnalités du plugin

Une fois activé, le plugin effectue automatiquement sur **l'espace public uniquement** une mise en conformité DSFR :

 * des raccourcis typographiques SPIP (tableaux, liens, notes...).
 * de certaines balises SPIP (`#NOTES`, `#TRI`...)

Le plugin ajoute aussi plusieurs fonctionnalités spécifiques au DSFR. Consultez la documentation du
plugin pour de plus amples informations au sujet de ces nouvelles fonctionnalités.


### Nouvelles balises SPIP

 * `#DSFR`
 * `#DSFR_COULEURS`
 * `#DSFR_ICONE`
 * `#DSFR_ICONES`
 * `#DSFR_PICTOGRAMME`
 * `#DSFR_PICTOGRAMMES`
 * `#DSFR_PLACEHOLDER`
 * `#DSFR_UNIQID`
 * `#DSFR_VERSION`

### Composants DSFR disponibles sous forme de squelette SPIP

Vous pouvez inclure dans vos squelettes des composants DSFR directement avec la syntaxe d'inclusion de SPIP
`<INCLURE{fond=dsfr_composants/nom_du_composant}>` ou `#INCLURE{fond=dsfr_composants/nom_du_composant}` (en statique).
Consultez la documentation du plugin pour connaître les paramètres d'inclusions à utiliser pour afficher un composant.

 * `accordeon`
 * `alerte`
 * `badge`
 * `bandeau_d_information_importante`
 * `barre_de_recherche`
 * `bouton`
 * `carte`
 * `citation`
 * `fil_d_ariane`
 * `gestionnaire_de_consentement`
 * `groupe_d_accordeons`
 * `groupe_de_badges`
 * `groupe_de_boutons`
 * `groupe_de_liens`
 * `groupe_de_tags`
 * `icone`
 * `lien`
 * `lien_d_evitement`
 * `mise_en_avant`
 * `mise_en_exergue`
 * `navigation_principale`
 * `pagination` (modèle SPIP)
 * `parametre_d_affichage`
 * `pictogramme`
 * `sommaire`
 * `tableau`
 * `tag`
 * `tuile`