<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_COULEURS` qui renvoit un tableau
 * de la liste de toutes les couleurs de la palette DSFR disponibles.
 * 
 * La liste peut être filtrée avec un paramètre.
 * 
 * @example
 *     `#DSFR_COULEURS`
 *     `#DSFR_COULEURS{illustrative}` / `#DSFR_COULEURS{categorie=illustrative}`
 *     `#DSFR_COULEURS{categorie=systeme}`
 * 
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `#DSFR_COULEURS{liste=categorie}`
 *     `#DSFR_COULEURS{liste=couleur}`
 **/
function balise_DSFR_COULEURS_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if ( !$arg ) {
		$p->code = 'dsfr_liste_couleurs()';
	} else {
		$p->code = 'dsfr_liste_couleurs((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}