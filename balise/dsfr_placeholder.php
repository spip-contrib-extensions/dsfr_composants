<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_PLACEHOLDER` qui cherche un placeholder DSFR dans les chemins
 * connus de SPIP et retourne son chemin complet depuis la racine.
 * 
 * La référence unique du placeholder est à fournir en paramètre.
 * 
 * La recherche est faite le dossier `dsfr_placeholders/`.
 * 
 * Si aucune extension n'est précisée, la recherche s'effectuera dans l'ordre suivant :
 *  - `dsfr_placeholders/$reference.svg`
 *  - `dsfr_placeholders/$reference.webp`
 *  - `dsfr_placeholders/$reference.png`
 *  - `dsfr_placeholders/$reference.gif`
 *  - `dsfr_placeholders/$reference.jpg`
 * 
 * Retourne une chaîne vide si le placeholder n'est pas trouvé.
 * 
 * @example
 *     `#DSFR_PLACEHOLDER{1x1}`
 *     `#DSFR_PLACEHOLDER{9x16}`
 *     `#DSFR_PLACEHOLDER{16x9}`
 *     `#DSFR_PLACEHOLDER{1x1.png}`
 *     `#DSFR_PLACEHOLDER{9x16.png}`
 *     `#DSFR_PLACEHOLDER{16x9.png}`
 * 
 * @see documentation/dossiers/dsfr_placeholders
 **/
function balise_DSFR_PLACEHOLDER_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if (!$arg) {
		$msg = ['zbug_balise_sans_argument', ['balise' => ' DSFR_PLACEHOLDER']];
		erreur_squelette($msg, $p);
	} else {
		$p->code = 'dsfr_placeholder((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}