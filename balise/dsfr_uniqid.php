<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise dynamique `#DSFR_UNIQID` qui permet de créé aléatoirement un identifiant unique
 * avec une longueur minimum de 4 caractères (sinon 13 caractères par défaut).
 * 
 * ATTENTION : cette fonction ne garantit pas l'unicité de la valeur retournée.
 * Avec des identifiants de petite longueur. Il y a de forte chance qu'il ne soit pas unique.
 * 
 * Une longueur peut être fourni en paramètre.
 * Si la longueur est égal à `-1`, retourne le dernier identifiant créé
 * 
 * @example
 *     `[(#DSFR_UNIQID)]`
 *     `[(#DSFR_UNIQID{4})]`
 * 
 *     // retourne le dernier identifiant créé
 *     `[(#DSFR_UNIQID{-1})]`
 * 
 *     // ATTENTION : un attribut id `HTML5` doit toujours commencer par un caractère alphanumérique
 *     // et `#DSFR_UNIQID` peut renvoyer un identifiant qui commence par caractère numérique
 *     // pour éviter un attribut id invalide, préfixez la balise
 *     `<div[ id="dsfr_(#DSFR_UNIQID{8})"]>...</div>`
 * 
 * @see documentation/fonctions/dsfr_uniqid
 **/
function balise_DSFR_UNIQID_dist($p) {
	return calculer_balise_dynamique($p, 'DSFR_UNIQID', []);
}

/**
 * Partie "statique" de la balise #DSFR_UNIQID.
 * Calculs de paramètres de contexte automatiques pour la balise concernée.
 *
 * @param array $args
 *     Liste des arguments demandés obtenus du contexte
 * @param array $context_compil
 *     Tableau d'informations sur la compilation
 *
 * @return array
 **/
function balise_DSFR_UNIQID_stat(array $args, array $context_compil): array {
	$longueur = $args[0] ?? '';

	return [$longueur];
}

/**
 * Partie "dynamique" de la balise #DSFR_UNIQID.
 *
 * @return string
 **/
function balise_DSFR_UNIQID_dyn($longueur): string {
	return dsfr_uniqid($longueur);
}