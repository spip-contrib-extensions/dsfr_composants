<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_PICTOGRAMMES` qui renvoit un tableau
 * de la liste de tous les pictogrammes DSFR disponibles.
 * 
 * La liste peut être filtrée avec un paramètre.
 * 
 * @example
 *     `#DSFR_PICTOGRAMMES`
 *     `#DSFR_PICTOGRAMMES{buildings}` / `#DSFR_PICTOGRAMMES{categorie=buildings}`
 *     `#DSFR_PICTOGRAMMES{categorie=document}`
 * 
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `#DSFR_PICTOGRAMMES{liste=categorie}`
 *     `#DSFR_PICTOGRAMMES{liste=type}`
 * 
 * @see documentation/composants/pictogramme
 * @see documentation/dossiers/dsfr_pictogrammes
 **/
function balise_DSFR_PICTOGRAMMES_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if ( !$arg ) {
		$p->code = 'dsfr_liste_pictogrammes()';
	} else {
		$p->code = 'dsfr_liste_pictogrammes((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}