<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_ICONE` qui cherche une icône DSFR dans les chemins
 * connus de SPIP et retourne son chemin complet depuis la racine.
 * 
 * La référence unique de l'icône est à fournir en paramètre.
 * 
 * La recherche est faite en premier dans le dossier `dsfr_icones/` 
 * puis dans le dossier `icons/` de la librairie DSFR.
 * 
 * Retourne une chaîne vide si l'icône n'est pas trouvée.
 * 
 * @example
 *     `#DSFR_ICONE{reference_de_votre_icone}`
 *     `#DSFR_ICONE{warning-fill}`
 *     `#DSFR_ICONE{error-fill}`
 *     `#DSFR_ICONE{account-fill}`
 * 
 * @see documentation/composants/icone
 * @see documentation/dossiers/dsfr_icones
 **/
function balise_DSFR_ICONE_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if (!$arg) {
		$msg = ['zbug_balise_sans_argument', ['balise' => ' DSFR_ICONE']];
		erreur_squelette($msg, $p);
	} else {
		$p->code = 'dsfr_icone((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}