<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR` qui cherche un fichier dans les chemins connus de SPIP 
 * de la librairie DSFR et retourne son chemin complet depuis la racine.
 *
 * Retourne une chaîne vide si le fichier n'est pas trouvé.
 *
 * @example
 *     `#DSFR{chemin/vers/fichier.ext}`
 *     `#DSFR{favicon/favicon.svg}`
 *     `[<img src="(#DSFR{favicon/favicon.svg})">]`
 *     `[<img src="(#DSFR{favicon/favicon.svg}|timestamp)">]`
 **/
function balise_DSFR_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if (!$arg) {
		$msg = ['zbug_balise_sans_argument', ['balise' => ' DSFR']];
		erreur_squelette($msg, $p);
	} else {
		$p->code = 'dsfr((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}