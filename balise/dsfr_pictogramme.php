<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_PICTOGRAMME` qui cherche un pictogramme DSFR dans les chemins
 * connus de SPIP et retourne son chemin complet depuis la racine.
 * 
 * La référence unique du pictogramme est à fournir en paramètre.
 * 
 * La recherche est faite en premier dans le dossier `dsfr_pictogrammes/` 
 * puis dans le dossier `artwork/pictograms/` de la librairie DSFR.
 * 
 * Retourne une chaîne vide si le pictogramme n'est pas trouvé.
 * 
 * @example
 *     `#DSFR_PICTOGRAMME{reference_du_pictogramme}`
 *     `#DSFR_PICTOGRAMME{dossier_du_pictogramme/nom_du_pictogramme}`
 *     `#DSFR_PICTOGRAMME{buildings/city-hall}`
 *     `#DSFR_PICTOGRAMME{document/passport}`
 * 
 * @see documentation/composants/pictogramme
 * @see documentation/dossiers/dsfr_pictogrammes
 **/
function balise_DSFR_PICTOGRAMME_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if (!$arg) {
		$msg = ['zbug_balise_sans_argument', ['balise' => ' DSFR_PICTOGRAMME']];
		erreur_squelette($msg, $p);
	} else {
		$p->code = 'dsfr_pictogramme((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}