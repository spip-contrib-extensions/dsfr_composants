<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_VERSION` qui affiche le numéro de la version actuelle 
 * de la librairie DSFR utilisée.
 * 
 * @example
 *     `[DSFR v(#DSFR_VERSION)]`
 **/
function balise_DSFR_VERSION_dist($p) {
	$p->code = 'dsfr_version()';

	$p->interdire_scripts = false;

	return $p;
}