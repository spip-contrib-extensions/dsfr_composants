<?php
/**
 * Balise SPIP du plugin
 *
 * @author		Jonathan OCHEJ
 * @license		GPL - https://www.gnu.org/licenses/gpl-3.0.html
 **/
if ( !defined('_ECRIRE_INC_VERSION') ) {
	return;
}

/**
 * Balise `#DSFR_ICONES` qui renvoit un tableau
 * de la liste de tous les icônes DSFR disponibles.
 * 
 * La liste peut être filtrée avec un paramètre.
 * 
 * @example
 *     `#DSFR_ICONES`
 *     `#DSFR_ICONES{buildings}` / `#DSFR_ICONES{categorie=buildings}`
 *     `#DSFR_ICONES{categorie=communication}`
 * 
 *     // cas spécial qui renvoit une liste de toutes les valeurs concernées
 *     `#DSFR_ICONES{liste=categorie}`
 *     `#DSFR_ICONES{liste=type}`
 * 
 * @see documentation/composants/icone
 * @see documentation/dossiers/dsfr_icones
 **/
function balise_DSFR_ICONES_dist($p) {
	$arg = interprete_argument_balise(1, $p);
	if ( !$arg ) {
		$p->code = 'dsfr_liste_icones()';
	} else {
		$p->code = 'dsfr_liste_icones((string)' . $arg . ')';
	}

	$p->interdire_scripts = false;

	return $p;
}