# Dossier `dsfr_pictogrammes/`

Dossier des pictogrammes DSFR.


## Surcharger un pictogramme

Ce dossier peut être `surchargé`. Créez le dossier `dsfr_pictogrammes/` dans le dossier
de votre propre plugin ou dans votre dossier `squelettes/` de SPIP. Vous pourrez ensuite
ajouter vos propres pictogrammes dans ce dossier qui seront prioritaires sur les pictogrammes
de la librairie DSFR.
Veillez à conserver la même arborescence du dossier `artwork/pictograms/` de la librairie DSFR
pour pouvoir surcharger un pictogramme.


## Ajouter un nouveau pictogramme

Vous pouvez compléter la liste des pictogrammes disponibles en ajoutant vos propres pictogrammes au 
format `reference_de_votre_pictogramme.svg` dans le dossier `dsfr_pictogrammes/`.
Vous pourrez alors utiliser ce nouveau pictogramme dans vos squelettes avec la balise
`#DSFR_PICTOGRAMME{reference_de_votre_pictogramme}`. La référence du pictogramme pourra aussi être utilisée
en paramètre de certains composants DSFR.

### Référence

**Attention**, contrairement au icônes, la référence d'un pictogramme inclue aussi le sous-dossier
dans lequel vous avez placé votre fichier de pictogramme. Par exemple, un pictogramme ayant pour nom
de fichier `mon_picto.svg` placé dans le sous-dossier `dsfr_pictogrammes/mon_sous_dossier/` aura pour 
référence `mon_sous_dossier/mon_picto` : `#DSFR_PICTOGRAMME{mon_sous_dossier/mon_picto}`.

### Catégories

Si besoin vous pouvez catégoriser vos pictogrammes en les plaçants dans différents sous-dossiers.
Un pictogramme placé directement à la racine du dossier `dsfr_pictogrammes/` sera considérée comme
sans catégorie. Vous pouvez ainsi facilement lister les pictogrammes d'une catégorie avec la
balise `#DSFR_PICTOGRAMMES{categorie=nom_de_votre_sous_dossier}`.

### Cache SPIP

La liste des pictogrammes étant mise en cache, veuillez vider le cache de SPIP pour appliquer 
les changements du dossier `dsfr_pictogrammes/`.


## Documentation

@see documentation/balises/dsfr_pictogramme
@see documentation/composants/pictogramme